

1. /app/pages/pages.routing.ts

{
  path: '###kebab###',
  loadChildren: 'app/pages/###kebab###/###kebab###.module####Camel###Module',
}


2. /app/pages/pages.menu.ts

{
	path: '###kebab###',
	data: {
	  menu: {
	    title: 'Quản lý ###label###',
	    icon: 'fa fa-comments-o',
	    selected: false,
	    expanded: false,
	    order: 6,
	    pathMatch: 'prefix'
	    //id: MenuAuthen.LIST_MENU_QUAN_LY_###UPPER_CASE###
	  }
	}
}