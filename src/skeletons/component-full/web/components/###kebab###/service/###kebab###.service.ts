import {Injectable} from '@angular/core';
import {Http, RequestOptions, ResponseContentType} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {UrlVariable} from "app/util/variable";
import {LogService} from "app/util/logHelper";
import {Observable} from 'rxjs';
import {###Camel###} from "../model/###kebab###.model";

/*
* @author ###author###
*/

@Injectable()
export class ###Camel###Service {

    private apiUrl = ###apiUrl###

    constructor(private http: Http, private logger: LogService) {}

    /**
     * @author ###author###
     * @description lấy danh sách hoặc tìm kiếm
     */
    public search = (searchBody, page: number, perPage: number): Promise<{data: ###Camel###[], length: number}> => {
        let body = Object.assign({}, searchBody);
        body['per_page'] = perPage;
        body['page'] = page;
        return this.http.post(`${this.apiUrl}/search`, body)
            .toPromise()
            .then(response => {
                return response.json();
            })
            .then(jsonData => {
                return {
                    data: jsonData.result,
                    length: jsonData.number_of_all_data.count
                }
            })
            .then(jsonData => {
                let arrayNoiDung = [];
                jsonData.data.forEach(_d => {
                    @@@if(hasStatus)@@@
                    if(typeof(_d.###status_snake###) === "boolean"){
                        _d.###status_snake### = _d.###status_snake###? 1 : 0;
                    }
                    @@@/if@@@
                    let _da: ###Camel### = Object.assign(new ###Camel###(), _d);
                    arrayNoiDung.push(_da);
                })
                jsonData.data = arrayNoiDung;
                return jsonData;
            })
            .catch(error => {
                console.error('ERROR: ###Camel###Service.search()', error);
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * @description dùng để update
     */
    public update = (body): Promise<any> => {
        return this.http.post(`${this.apiUrl}/update`, body)
            .toPromise()
            .then(value => value.json())
            .catch(error => {
                console.error('ERROR: ###Camel###Service.update() ', error);
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
    * @author ###author###
    * @description dùng để update 1 dòng dữ liệu
    */
    public create = (body): Promise<any> => {
        return this.http.post(`${this.apiUrl}/create`, body)
            .toPromise()
            .then(value => value.json().result)
            .catch(error => {
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     */
    public getOne = (id): Promise<any> => {
        return this.http.get(`${this.apiUrl}/getone?###id_snake###=${id}`)
            .toPromise()
            .then(value => value.json().result)
            .catch(error => {
                console.error('ERROR: ###Camel###Service.getOne()', error);
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
    * @author ###author###
    * @description dùng để delete 1 hay nhiều
    */
    public delete = (###id_snake###s: number[]): Promise<any> => {
        return this.http.post(`${this.apiUrl}/delete`, {###id_snake###s: ###id_snake###s})
            .toPromise()
            .then(value => value.json())
            .catch(error => {
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
    * @author ###author###
    * @description dùng để xóa theo filter
    */
    public deleteFilter = (body): Promise<any> => {
        return this.http.post(`${this.apiUrl}/delete-by-filter`, body)
            .toPromise()
            .then(value => value.json())
            .catch(error => {
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * @description dùng để update 1
     */
    public getsFilter = (body): Promise<any> => {
        return this.http.post(`${this.apiUrl}/gets-by-filter`, body)
            .toPromise()
            .then(value => value.json().result)
            .catch(error => {
                this.logger.saveLog(error)
                return Promise.reject(error);
            });
    }

    /**
       * @author ###author###
       * Build excel trả về buffer để download
       */
    buildExcel(body): Promise<any> {
        return this.http.post(`${UrlVariable.URL_API_CAU_HINH_CHUNG}/api/cauhinhchung/buildexcel`, body)
            .toPromise()
            .then(response => {
                return response.text();
            })
            .catch(error => {
                console.error('ERROR: ###Camel###Service.buildExcel()', error);
                return Promise.reject(error);
            })
    }

    /**
      * @author ###author###
      * Decode excel
      */
    decodeExcel(body): Promise<any> {
        return this.http.post(`${UrlVariable.URL_API_CAU_HINH_CHUNG}/api/cauhinhchung/decode`, body)
            .toPromise()
            .then(response => {
                return response.json();
            })
            .catch(error => {
                console.error('ERROR: ###Camel###Service.decodeExcel()', error);
                return Promise.reject(error);
            })
    }

}
