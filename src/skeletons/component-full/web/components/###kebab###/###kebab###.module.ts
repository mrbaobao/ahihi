import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from "app/shared/shared.module";
import { RouterModule } from "@angular/router";
import { TooltipModule } from "ngx-bootstrap";

import { Page###Camel###ListComponent } from './admin/###kebab###-list/page-###kebab###-list.component';

import { ###Camel###Service } from "app/components/###kebab###/service/###kebab###.service";

import { ComSearchBar###Camel###Component } from './admin/###kebab###-list/shared/com/search/com-search-bar-###kebab###.component';
@@@if(hasStatus)@@@
import { Com###StatusCamel###Component } from './admin/###kebab###-list/shared/com/###status-kebab###/com-###status-kebab###.component';
@@@/if@@@
import { Page###Camel###DetailComponent } from './admin/###kebab###-list/shared/page/###kebab###-detail/page-###kebab###-detail.component';

import { Modal###Camel###CreateComponent } from './admin/###kebab###-list/shared/modal/###kebab###-create/modal-###kebab###-create.component';
import { Modal###Camel###UpdateComponent } from './admin/###kebab###-list/shared/modal/###kebab###-update/modal-###kebab###-update.component';
import { Modal###Camel###DetailComponent } from './admin/###kebab###-list/shared/modal/###kebab###-detail/modal-###kebab###-detail.component';

import { Page###Camel###CreateComponent } from './admin/###kebab###-list/shared/page/###kebab###-create/page-###kebab###-create.component';
import { Page###Camel###UpdateComponent } from './admin/###kebab###-list/shared/page/###kebab###-update/page-###kebab###-update.component';
import { Page###Camel###ImportComponent } from './admin/###kebab###-list/shared/page/###kebab###-import/page-###kebab###-import.component';

/*
* @author ###author###
*/
@NgModule({
  imports: [SharedModule, RouterModule, TooltipModule.forRoot()],
  declarations: [
  Page###Camel###ListComponent, 
  ComSearchBar###Camel###Component, Modal###Camel###DetailComponent@@@if(hasStatus)@@@, Com###StatusCamel###Component@@@/if@@@,
   Page###Camel###DetailComponent, Modal###Camel###CreateComponent, Modal###Camel###UpdateComponent, 
   Page###Camel###CreateComponent, Page###Camel###UpdateComponent, Page###Camel###ImportComponent
   ],
  providers: [###Camel###Service]
})
export class ###Camel###Module { }
