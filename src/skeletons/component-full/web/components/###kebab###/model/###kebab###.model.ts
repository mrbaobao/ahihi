
/*
* @author ###author###
*/

let _checked = new WeakMap();
export class ###Camel### {
###db-col-list###
    constructor() {
    }
    public get isChecked() {
        return _checked.get(this) || false;
    }

    public set isChecked(value) {
        _checked.set(this, value);
    }
}
