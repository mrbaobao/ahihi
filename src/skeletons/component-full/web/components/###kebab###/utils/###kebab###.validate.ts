import * as moment from 'moment';
/*
*  @author ###author###
*/
export class ###Camel###Validation {

    static message_error = {
        dien_thoai: 'Sai định dạng số điện thoại',
        ngay_sinh: 'Sai định dạng ngày sinh',
        ca_hoc: 'Sai định dạng ca học',
        null: 'không được để trống',
        thu: 'Sai định dạng thứ',
        email: 'Sai định dạng email',
        number: 'Phải là số'
    }

    /**
   * kiểm tra số điện thoại
   */
    static validationPhoneNumber = (value) => {
        if (!###Camel###Validation.validationNull(value)) {
            return true;
        }
        var filter = /[0-9-+]+/;
        if (filter.test(value)) {
            return true;
        }
        return false;
    }

    /**
     * kiểm tra ngày sinh
     */
    static validationNgaySinh = (value) => {
        if (!###Camel###Validation.validationNull(value)) {
            return true;
        }
        /** check đúng định dạng dd/mm/yyyy và d/m/yyyy
         *  khi dùng moment check thì có 1 số trường hợp vẫn đúng ví dụ 12-09-99
         *   -> check thêm 1 lớp nữa 2 số - 2 số - 4 số
         */
        if (moment(value, 'DD/MM/YYYY').isValid()) {
            if (/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/.test(value) == true) {
                return true;
            }
        }
        if (moment(value, 'D/M/YYYY').isValid()) {
            if (/\b\d{1,2}[\/-]\d{1,2}[\/-]\d{4}\b/.test(value) == true) {
                return true;
            }
        }
        // if (moment(value, 'DD/MM/YYYY').isValid()) {
        //     return true;
        // }
        return false;
    }

    /**
     * kiểm tra định dạng email
     */
    static validationEmail = (value) => {
        if (!###Camel###Validation.validationNull(value)) {
            return true;
        }
        let regex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
        if (!regex.test(value)) {
            return false;
        }
        return true;
    }

    /**
    * kiểm tra null
    * có 2 kết quả: true (đúng), false (sai)
    * trường hợp 1: kiểm tra nếu bị undefined -> false
    */
    static validationNull = (value) => {
        if (value === undefined) {
            return false;
        }
        if (value === null || value.trim().length === 0) {
            return false;
        }
        return true;
    }

    /**
     * kiem tra phai so ko
     *  ko phải số báo lỗi
     */
    static validationNumber = (value) => {
        if (!###Camel###Validation.validationNull(value)) {
            return true;
        }
        if (isNaN(value) == false) {
            return true;
        }
        return false;
    }

    /**
     * thông báo sai định dạng
     */
    static thongBaoSaiDinhDang = (property, header?: string): string => {
        return (header ? header + " " : "") + ###Camel###Validation.message_error[property];
    }

}