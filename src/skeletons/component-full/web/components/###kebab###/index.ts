/*
	@author ###author###
	Public pages
*/
//List
export * from './admin/###kebab###-list/page-###kebab###-list.component';
//Create
export * from './admin/###kebab###-list/shared/page/###kebab###-create/page-###kebab###-create.component';
//Detail
export * from './admin/###kebab###-list/shared/page/###kebab###-detail/page-###kebab###-detail.component';
//Update
export * from './admin/###kebab###-list/shared/page/###kebab###-update/page-###kebab###-update.component';
//Import
export * from './admin/###kebab###-list/shared/page/###kebab###-import/page-###kebab###-import.component';