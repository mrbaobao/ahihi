import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {IAddOnComponent} from 'app/shared/add-on.interface';
import {ManageStateService} from 'app/shared/manage-state.service';
import {Config} from 'app/components/###kebab###/config';

/*
* @author ###author###
*/

@Component({
    selector: 'com-###status-kebab###',
    template: '<select2 [value]="_statusValue" [data]="_data" [options]="_options" (valueChanged)="changed($event)"></select2>',
})
export class Com###StatusCamel###Component implements OnInit, IAddOnComponent {
    private _data: Array<any> = [];
    private _options;
    private _statusValue;
    private _keyValue = '###kebab###:status';
    @Output() valueUpdated = new EventEmitter();

    constructor(private manageStateService: ManageStateService) {
        this.loadState();
    }

    ngOnInit() {
        this._options = {
            multiple: true,
            closeOnSelect: false,
            allowClear: true,
            placeholder: '###status-field-label###',
            dropdownAutoWidth: true,
            width: '200px',
            height: '20px'
        }

        /**
         * id là giá trị chọn lấy được
         * text là giá trị cho người dùng thấy được
         */
        this._data = [
###status-id-text-list###
        ];
    }

    public getState() {
        return {name: '###status_snake###', value: this._statusValue};
    }

    private changed(e) {
        if (this._statusValue === e.value) {
            return;
        }
        this._statusValue = e.value;
        this.saveState();
        this.valueUpdated.emit(e.value);
    }

    private saveState() {
        this.manageStateService.save(this._keyValue, this._statusValue);
    }

    private loadState() {
        this._statusValue = this.manageStateService.load(this._keyValue);
    }

}
