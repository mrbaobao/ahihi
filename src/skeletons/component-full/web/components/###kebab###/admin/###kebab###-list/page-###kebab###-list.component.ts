import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {Router} from "@angular/router";
import {ModalDirective} from 'ngx-bootstrap';
import {IAddOnComponent} from 'app/shared/add-on.interface';
import * as _ from 'lodash';
import { promiseFor } from "app/util/promise-for";
import {default as swal} from 'sweetalert2';
import {saveAs} from 'file-saver';
import {Alert} from 'app/util/alert';
import {ToastsManager} from 'ng2-toastr';
import {Config} from 'app/components/###kebab###/config';
import {ManageStateService} from 'app/shared/manage-state.service';
import {PagingComponent} from "app/shared/paging/paging.component";


import {ComSearchBar###Camel###Component} from "./shared/com/search/com-search-bar-###kebab###.component";
@@@if(hasStatus)@@@
import {Com###StatusCamel###Component} from "./shared/com/###status-kebab###/com-###status-kebab###.component";
@@@/if@@@
import {Modal###Camel###DetailComponent} from "./shared/modal/###kebab###-detail/modal-###kebab###-detail.component";
import {Modal###Camel###CreateComponent} from './shared/modal/###kebab###-create/modal-###kebab###-create.component';
import {Modal###Camel###UpdateComponent} from './shared/modal/###kebab###-update/modal-###kebab###-update.component';

import {###Camel###Service} from "app/components/###kebab###/service/###kebab###.service";
import {###Camel###} from "app/components/###kebab###/model/###kebab###.model";


/*
* @author ###author###
*/

@Component({
    selector: 'page-###kebab###-list',
    templateUrl: './page-###kebab###-list.component.html',
    styleUrls: ['./page-###kebab###-list.component.scss']
})
export class Page###Camel###ListComponent implements OnInit {
    private _data: Promise<###Camel###[]>;
    private _total: number = 0;
    private _itemsPerPage: number;
    private _currentPage: number;
    private _keyValue = 'admin###okman###list:';
    private _isLoaded = false;
    private _selectedRow: Number;

    private _checkedListID: Set<any> = new Set();
    private isCheckedAll = false;
    private body = {};
    private _addOnComponents: Array<IAddOnComponent> = [];

    @ViewChild('errorModal') errorModal;
    @ViewChild('pbmodal') pbModal;
    @ViewChild(ComSearchBar###Camel###Component) private _searchComponent: ComSearchBar###Camel###Component;
    @ViewChild(PagingComponent) private _pagingComponent: PagingComponent;
    @@@if(hasStatus)@@@
    @ViewChild(Com###StatusCamel###Component) _status###StatusCamel###: Com###StatusCamel###Component;
    private ###StatusCamel###MapThuan = Config.###StatusCamel###MapThuan;
    @@@/if@@@
    @ViewChild(Modal###Camel###DetailComponent) private _###okman###detail: Modal###Camel###DetailComponent;
    @ViewChild(Modal###Camel###CreateComponent) private _###okman###create: Modal###Camel###CreateComponent;
    @ViewChild(Modal###Camel###UpdateComponent) private _###okman###update: Modal###Camel###UpdateComponent;

    
    constructor(
        private service: ###Camel###Service,
        private manageStateService: ManageStateService,
        private router: Router,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this._addOnComponents = [
            this._searchComponent,
            this._pagingComponent@@@if(hasStatus)@@@,
            this._status###StatusCamel###@@@/if@@@
        ];
        this.loadState();
        this._data = this.getData().then(result => {
            // console.log(result);
            this._isLoaded = true;
            this._total = result.length;
            return result.data;
        });
    }
    private identify(index, item: ###Camel###) {
        return item.###id_snake###;
    }

    /**
     * tạo số thứ tự
     */
    private calculateAlphaNumber(): number {
        return (this._currentPage - 1) * this._itemsPerPage;
    }

    /**
     * gọi api lấy danh sách dữ liệu
     */
    private getData(): Promise<any> {
        this._isLoaded = false;
        this.body = {};
        this._selectedRow = -1;
        this._addOnComponents.forEach(ac => {
            const state = ac.getState();
            if (!_.isNil(state.value) && !_.isEmpty(state.value) && state.name !== 'page') {
                this.body[state.name] = state.value;
            }
            if (state.name === 'page' && state.value) {
                this._currentPage = state.value;
                this.saveState();
            }
        });
        return this.service.search(this.body, this._currentPage, this._itemsPerPage).then((result) => {
            this._isLoaded = true;
            return result;
        })
            .catch(error => {
                console.log(error);
                if (error.status == 400) {
                    this.notifyEmpty()
                } else {
                    this.pushError()
                }

                return {};
            });
    }
    notifyEmpty = () => {
        const message = "Không tìm thấy kết quả tìm kiếm. Vui lòng thử lại từ khóa khác!";
        const title = 'Not found';
        const buttonBackOption = {
            text: 'OK',
            fn: () => {
                console.log(this.errorModal);
                this.errorModal.hide()
            }
        }
        this.errorModal.show(title, message, buttonBackOption);
    }
    pushError = () => {
        const message = `
                    Hiện tại không kết nối được tới server, xin hãy kiểm tra thiết bị có kết nối internet hay không.
                    Nếu có internet mà lỗi này vẫn hiện xin hãy báo lại với chúng tôi`;
        const title = 'Error';
        const buttonBackOption = {
            text: 'Trở lại dashboard',
            fn: this.goToDashBoard
        }
        this.errorModal.show(title, message, buttonBackOption);
    }
    /**
     * trở về trang chủ
     */
    private goToDashBoard = () => {
        this.router.navigate(['/admin/dashboard']);
    }

    /**
     * cập nhật danh sách sự cố theo phân trang
     */
    private updatePerPage() {
        this.saveState();
        this.stateChange();
    }

    /**
     * lưu giá trị cookie
     */
    private saveState() {
        this.manageStateService.save(this._keyValue + 'perPage', this._itemsPerPage);
        this.manageStateService.save(this._keyValue + 'currentPage', this._currentPage);
    }

    /**
     * load giá trị cookie
     */
    private loadState() {
        this._itemsPerPage = this.manageStateService.load(this._keyValue + 'perPage') || 10;
        this._currentPage = this.manageStateService.load(this._keyValue + 'currentPage') || 1;
    }

    private stateChange() {
        this._data = this.getData().then(result => {
            // console.log(result);
            this._total = result.length;
            return result.data;
        });
    }

    /**
       * chọn 1 dòng
       */
    private selectRow(index) {
        this._selectedRow = index;
    }

    /**
     * xóa chọn tất cả
     */
    private clearCheckedAll() {
        this._data.then((_array) => {
            _array.forEach(_d => {
                _d.isChecked = false;
            })
        })
        this._checkedListID.clear();
    }

    /**
     * chọn tất cả
     */
    private checkedAll() {
        if (this.isCheckedAll) {
            this._data.then(_array => {
                _array.forEach(_v => {
                    this._checkedListID.add(_v.###id_snake###);
                    _v.isChecked = this.isCheckedAll;
                })
            })
        } else {
            this.clearCheckedAll();
        }

    }

    private checkBoxChecked(checked: boolean, row: ###Camel###) {
        if (checked) {
            this._checkedListID.add(row.###id_snake###);
        } else {
            this._checkedListID.delete(row.###id_snake###);
        }
    }

    /**
    * xóa 1 dữ liệu
    */
    private delete(id) {
        Alert.showWaring(
            'Cảnh báo xóa dữ liệu',
            'Dữ liệu xóa thì không thể lấy lại',
            'Xóa',
            'Thôi'
        ).then(() => {
            this.service.delete([id])
                .then(() => {
                    this._checkedListID.delete(id);
                    return this._data;
                })
                .then(value => {
                    this.toastr.success('Xóa thành công');
                    _.remove(value, (vl: any) => {
                        return vl.###id_snake### === id;
                    });
                })
                .catch(error => {
                    this.toastr.error('Xóa thất bại');
                })
        }).catch(err => {});
    }

    /**
    * Xóa theo chọn
    */
    private deleteByChecked() {
        const PER_DELETE = 5;
        Alert.showDeleteAlertWithCancel(
            'Có chắc muốn xóa dữ liệu không?',
            'Dữ liệu sẽ bị xóa vĩnh viễn không thể quay lại')
            .then(() => {
                // chuyển từ this._checkedListID: Set -> listId: Array
                let listId = Array.from(this._checkedListID);
                // nếu có chọn thì bắt đầu xóa
                if (listId.length !== 0) {
                    let max = listId.length;
                    let value = 0;
                    // điều kiện dừng
                    let condition = (_array: Array<any>) => {
                        return _array.length > 0;
                    }
                    // hành động lặp
                    let action = (_array: Array<any>) => {
                        let _deleteArray = listId.splice(0, PER_DELETE);
                        return this.service.delete(_deleteArray)
                            .then((value) => {
                                this.toastr.success(`Xoá thành công`);
                                return listId;
                            }).catch(err => {
                                this.toastr.success(`Xoá thất bại`);
                            })
                    }

                    // thực thi thuật toán
                    promiseFor(condition, action, listId)
                        .then(() => {
                            this.stateChange();
                            this._checkedListID.clear();
                            this.isCheckedAll = false;
                        })
                } else {
                    swal('Bạn chưa chọn gì cả', 'Không có dữ liệu nào bị xóa', 'info');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    /**
     * xóa theo bộ lọc
     */
    deleteByFilter = () => {
        Alert.showDeleteAlertWithCancel(
            'Có chắc muốn xóa dữ liệu không?',
            'Dữ liệu sẽ bị xóa vĩnh viễn không thể quay lại')
            .then(() => {
                this.service.deleteFilter(this.body)
                    .then(value => {
                        this.toastr.success(`Xóa thành công`);
                        this._checkedListID.clear();
                        this.isCheckedAll = false;
                        this.stateChange();
                    }).catch(err => {
                        this.toastr.success(`Xoá thất bại`);
                    })
            }).catch(err => {
                console.log(err);
            })
    }

    /**
     * xem chi tiết bằng Modal
     */
    openDetailModal = (value) => {
        this._###okman###detail.show(value)
    }

    /**
     * Xem chi tiết sự cố bằng page
     */
    openDetailPage = (value) => {
        this.router.navigate(['/admin/###kebab###/chi-tiet-###kebab###', value]);
    }

    /**
     * Tạo bằng Modal
     */
    openCreateModal = () => {
        this._###okman###create.show();
    }

    /**
     * Tạo bằng page
     */
    openCreatePage = () => {
        this.router.navigate(['/admin/###kebab###/them-###kebab###']);
    }

    /**
     * Tạo  bằng Modal
     */
    openEditModal = (value) => {
        this._###okman###update.show(value);
    }

    /**
    * Tạo bằng Page
    */
    openUpdatePage = (value) => {
        this.router.navigate(['/admin/###kebab###/cap-nhat-###kebab###', value]);
    }

    /**
     * mở page import
     */
    openImport = () => {
        this.router.navigate(['/admin/###kebab###/import-###kebab###']);
    }
    /**
     * export excel
     */
    exportExcel() {
        let xuat_excel = [];

        /**
         * lấy danh sách sự cố theo bộ lọc
         */
        this.service.getsFilter(this.body)
            .then(value => {
                console.log(value)
                if (!value) {
                    this.toastr.error("không có gì để export");
                    return;
                }
                if (value.length == 0) {
                    this.toastr.error("không có gì để export");
                    return;
                }

                /**
                 * lấy tiêu đề excel
                 */
                xuat_excel.push(Config.ExportExcel.EXPORT_MAP.map(x => x.header));
                value.forEach(x => {
                    let r = [];

                    /**
                     * gom các giá trị tương ứng
                     */
                    Config.ExportExcel.EXPORT_MAP.forEach(element => {
                        let value = x[element.property];
                        if(element.property === "###status_snake###"){
                            @@@if(hasStatus)@@@
                            if(typeof(value) === "boolean"){
                                value = value? 1 : 0;
                            }
                            r.push(Config.###StatusCamel###MapThuan[value]);
                            @@@/if@@@

                        } else {
                            r.push(value);
                        }
                        
                    })
                    xuat_excel.push(r);
                });

                /**
                 * build thành file excel
                 */
                this.service.buildExcel({data: xuat_excel})
                    .then(result => {
                        let data = new Blob([this.convert(result.replace(/"/g, ''))]);
                        saveAs(data, Config.ExportExcel.EXPORT_FILE_NAME)
                    })
            }).catch(err => {
                this.toastr.error('Có lỗi xảy ra . không thể export file');
            })
    }

    /**
     * Decode base64 để download
     */
    convert(base64) {
        let binary_string = window.atob(base64);
        let len = binary_string.length;
        let bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }


}
