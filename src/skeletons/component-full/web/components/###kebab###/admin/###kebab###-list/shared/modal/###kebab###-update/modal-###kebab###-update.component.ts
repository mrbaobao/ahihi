import {Component, OnInit, Output, ViewChild, ViewContainerRef, EventEmitter} from '@angular/core';
import {ModalDirective} from 'ngx-bootstrap';
import {Config} from 'app/components/###kebab###/config';
import {ToastsManager} from 'ng2-toastr';

import {###Camel###} from "app/components/###kebab###/model/###kebab###.model";
import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';

/*
* @author ###author###
*/
@Component({
    selector: 'modal-###kebab###-update',
    templateUrl: './modal-###kebab###-update.component.html',
    styleUrls: ['./modal-###kebab###-update.component.scss']
})
export class Modal###Camel###UpdateComponent {
    @ViewChild('lgModal') public _modal: ModalDirective;
    @Output() success = new EventEmitter();
    private loaded = false;
    @@@if(hasStatus)@@@
    private select###StatusCamel### = Config.###StatusCamel###Map;
    @@@/if@@@
    private data: ###Camel### = new ###Camel###();
    //Block form
    private isBlocking: boolean = false;

    constructor(private service: ###Camel###Service,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager
    ) {}

    show(id) {
        this._modal.show();
        this.service.getOne(id)
            .then(value => {
                @@@if(hasStatus)@@@
                this.data.###status_snake### = value.###status_snake######status_value_suffix###;
                @@@/if@@@
                @@@if(!hasStatus)@@@
                this.data.###sample_snake### = value.###sample_snake###;
                @@@/if@@@
                this.data.###id_snake### = id;
                this.loaded = true;
            });
    }

    close = () => {
        this._modal.hide();
        setTimeout(() => {
            this.loaded = false;
        }, 50);
    }

    update = () => {
        if(this.isBlocking){
            return;
        }
        this.isBlocking = true;
        let body = {
            ###id_snake###: +this.data.###id_snake###,
            @@@if(hasStatus)@@@
            ###status_snake###: ###status_value_prefix###this.data.###status_snake###,
            @@@/if@@@
            @@@if(!hasStatus)@@@
            ###sample_snake###: this.data.###sample_snake###,
            @@@/if@@@
        }
        this.service.update(body)
            .then(value => {
                this.toastr.success("Cập nhật thành công");
                this.success.emit();
                this._modal.hide();
                this.isBlocking = false;
            }).catch(err => {
                this.toastr.error("Cập nhật thất bại");
                this.isBlocking = false;
            })

    }
}
