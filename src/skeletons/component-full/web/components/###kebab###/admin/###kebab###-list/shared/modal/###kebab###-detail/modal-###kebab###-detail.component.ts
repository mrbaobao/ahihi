import {Component, OnInit, ViewChild, EventEmitter, Output} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {Config} from 'app/components/###kebab###/config';

import {###Camel###} from "app/components/###kebab###/model/###kebab###.model";
import {###Camel###Service} from "app/components/###kebab###/service/###kebab###.service";
/*
* @author ###author###
*/
@Component({
    selector: 'modal-###kebab###-detail',
    templateUrl: './modal-###kebab###-detail.component.html',
    styleUrls: ['./modal-###kebab###-detail.component.scss']
})
export class Modal###Camel###DetailComponent {
    @ViewChild('lgModal') public _modal: ModalDirective;
    /**
     * tạo loading khi load dữ liệu
     */
    private loaded: boolean = false;
    @@@if(hasStatus)@@@
    private select###StatusCamel### = Config.###StatusCamel###Map;
    private ###StatusCamel###MapThuan = Config.###StatusCamel###MapThuan;
    @@@/if@@@
    private data: ###Camel###;

    @Output() success = new EventEmitter()
    
    constructor(
        private service: ###Camel###Service) {
    }

    show = (id) => {
        this.data = new ###Camel###();
        this._modal.show();
        this.service.getOne(id)
            .then(value => {
                this.data.###id_snake### = value.###id_snake###;
                @@@if(hasStatus)@@@
                this.data.###status_snake### = value.###status_snake######status_value_suffix###;
                @@@/if@@@
                 @@@if(!hasStatus)@@@
                this.data.###sample_snake### = value.###sample_snake###;
                @@@/if@@@
                
                this.loaded = true;
            })
    }

    close = () => {
        this._modal.hide();
        setTimeout(() => {
            this.loaded = false;
        }, 50);
    }

}
