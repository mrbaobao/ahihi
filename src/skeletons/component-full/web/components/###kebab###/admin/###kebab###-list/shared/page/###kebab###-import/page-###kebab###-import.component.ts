import {Component, OnInit, ViewChild, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr/ng2-toastr';
import {Router} from "@angular/router";
import {saveAs} from 'file-saver';
import {Config} from 'app/components/###kebab###/config';


import {###Camel###} from 'app/components/###kebab###/model/###kebab###.model';
import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';
import {###Camel###Validation} from 'app/components/###kebab###/utils/###kebab###.validate';

/*
* @author ###author###
*/
@Component({
    selector: 'page-###kebab###-import',
    templateUrl: './page-###kebab###-import.component.html',
    styleUrls: ['./page-###kebab###-import.component.scss']
})
export class Page###Camel###ImportComponent implements OnInit {
    [x: string]: any;
    /**
     * Nếu true => Đang upload file
     */
    isLoading = false;

    /**
     * Biến lưu dữ liệu từ excel
     */
    private excelContent: any;

    /**
     * Nếu true => Đã upload file => Mở button import , mở thông tin về import
     */
    private isUploaded: boolean = false;

    /**
     * Nếu true => Đang insert dữ liệu => Khóa button import , mở progress bar
     */
    private isInserting: boolean = false;

    /**
     * Nếu true => Đang insert dữ liệu => Mở các button khác nhưng vẫn khóa button import
     */
    private isImported: boolean = true;


    /**
     * Nếu true => Có thể export dữ liệu => Mở button export
     */
    private canExport: boolean = false;


    /**
     * Mảng chứa các dòng lỗi
     */
    private errorRow: Array<any> = [];

    /**
     * Mảng chứa dữ liệu để export
     */
    private exportData: Array<any> = [];

    /**
     * Biến đếm số dònopenImportg thành công,thất bại
     */
    private successCount: number = 0;
    private errorCount: number = 0;

    /**
     * Biến đếm số dòng đã kiểm tra
     */
    private checked = 0;

    /**
     * mảng chứa dữ liệu bị lỗi
     */
    private exportErrorData = [];

    /**
     * biến check hoàn thành
     */
    private Finish = false;
    @ViewChild('fileUpload') fileUpload;
    @ViewChild('errorModal') errorModal;

    constructor(
        private router: Router,
        private toastr: ToastsManager,
        private vcr: ViewContainerRef,
        private service: ###Camel###Service,
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        window.addEventListener("beforeunload", function (e) {
            var confirmationMessage = "\o/";
            (e || window.event).returnValue = confirmationMessage; //Gecko + IE
            return confirmationMessage;                            //Webkit, Safari, Chrome
        });
    }

    /**
      * Kiểm tra đuôi file
      * @param filename tên file
      * @param required đuôi file cần
      */
    checkFileExtension(filename, required) {
        let parts = filename.split('.');
        return parts[parts.length - 1] === required;
    }

    /**
   * Bắt đầu đọc và mã hóa file
   * @param e File -> file người dùng chọn 
   */
    upload(e) {
        this.isUploaded = false;
        this.isInserting = false;
        this.errorRow = [];
        this.canExport = false;
        // Nếu không chọn file
        if (e.target.files.length === 0) {
            return;
        }
        /**
         * đọc file
         */
        var fReader = new FileReader();
        let file = e.target.files[0];
        // Kiểm tra đuôi file
        if (!this.checkFileExtension(file.name, 'xlsx')) {
            this.toastr.error('Chỉ nhận file có đuôi .xlsx')
            return;
        }
        let encoded = '';
        fReader.readAsDataURL(file);
        fReader.onloadend = (event: any) => {
            this.isLoading = true;
            /**
             * lấy giá trị đã được mã hóa đưa lên server giải mã
             */
            let encoded = event.target.result.replace('data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,', '');
            this.service.decodeExcel({file: encoded})
                .then(result => {
                    this.isLoading = false;

                    /**
                     * kiểm tra xem có cột nào không có không?
                     */
                    if (result.headers.indexOf(null) !== -1) {
                        this.toastr.error('Dữ liệu không đúng mẫu import');
                        return;
                    }

                    /**
                     *  kiểm tra mẫu trong file excel
                     */
                    for (let i = 0; i < Config.ImportExcel.HEADER_COLUMN.length; i++) {
                        if (result.headers.indexOf(Config.ImportExcel.HEADER_COLUMN[i]) === -1) {
                            this.toastr.error('Dữ liệu không đúng mẫu import');
                            return;
                        }
                    }
                    /** 
                     * Khởi tạo dữ liệu để bắt đầu import
                     */
                    this.checked = 0;
                    this.errorCount = 0;
                    this.successCount = 0;
                    this.excelContent = result.excel;
                    this.isUploaded = true;
                })
                .catch(err => {
                    this.toastr.error("Có lỗi xảy ra, xin bạn kiểm tra lại");
                })
        }
    }

    /**
     * insert vào database
     *  -> khởi tạo mảng chứa dữ liệu để xuất file
     *  -> kiểm tra và mapping từng dữ liệu đúng mẫu để insert vô db
     *       -> có lỗi -> push vô mảng báo lỗi
     *       -> thành công -> gửi lên api để insert -> push vô mảng insert thành công
     *  
     * -> nếu insert lên db xong nhưng có dữ liệu lỗi -> mở chức năng export để người dùng có thể
     * xuất dữ liệu lỗi đó ra 
     */
    insert = () => {
        
        let promise = [];
        /**
         * Mảng chứa dữ liệu xuất ra file 
         */
        this.exportData = [];
        this.isImported = false;
        this.isInserting = true;
        this.Finish = false;
        let i = 0;
        let insert = setInterval(() => {
            let row: ###Camel### = this.mapDL(this.excelContent[i]);
            // console.log(row);
            if (row['error']) {
                this.checked++;
                this.errorCount++;
                this.errorRow.push(row);
            } else {
                let body = {
                    @@@if(hasStatus)@@@
                    ###status_snake###: ###status_value_prefix###Config.###StatusCamel###MapNguoc[row.###status_snake###],
                    @@@/if@@@
                    @@@if(!hasStatus)@@@
                    ###sample_snake###: row.###sample_snake###,
                    @@@/if@@@
                }
                // console.log(row);
                // console.log(body);
                promise.push(this.service.create(body)
                    .then((res) => {
                        this.checked++;
                        // Nếu insert có lỗi
                        if (res.error) {
                            this.errorCount++;
                            row['error'] = res.error.message;
                            this.errorRow.push(row);
                            return;
                        }
                        // console.log(body);
                        this.successCount++;
                    })
                )
            }
            if (this.excelContent.length - 1 === i) {
                clearInterval(insert);
                // Sau khi insert xong hết và có dữ liệu lỗi => Mở chức năng export
                return Promise.all(promise)
                    .then(() => {
                        this.Finish = true;
                        this.isImported = true;
                        if (this.errorRow.length > 0) {
                            let headers = [];
                            // Push tên cột
                            Config.ExportExcel.ERROR.forEach(element => {
                                headers.push(element.header);
                            });
                            this.exportErrorData.push(headers);
                            // console.log(this.errorRow);
                            // Push dữ liệu
                            this.errorRow.forEach(row => {
                                // console.log(row);
                                let r = [];
                                Config.ExportExcel.ERROR.forEach(element => {
                                    // console.log(row);
                                    // console.log(element)
                                    r.push(row[element.property]);
                                    // console.log(r);
                                });
                                this.exportErrorData.push(r);

                            });
                            this.canExport = true;
                        }
                    })
            }
            i++;
        }, 2)
    }


    /**
     * mapping và kiểm tra giá trị
     * kiểm tra giá trị, nếu skip:
     *    -> false -> bắt buộc phải kiểm tra -> 
     *                 -> kiểm tra nếu notNull=true 
     *                      -> kiểm tra xem nó có null không
     *                            -> có thì bỏ vào thuộc tính báo lỗi
     *                            -> không thì kiểm tra tiếp yêu cầu còn lại
     *                  -> nếu notNull=false -> kiểm tra yêu cầu còn lại
     *   -> true -> bỏ qua 
     *   -------------> trước khi trả về check tồn tại 
     *                       -> nếu tồn tại trả về giá trị trim 2 đầu
     *                       -> nếu không tồn tại trả về null
     *
     */
    mapDL = (object): ###Camel### => {
        // console.log(object);
        let row = new ###Camel###();
        // this.logservice.Log("mapDL");
        Config.ImportExcel.VALIDATION_PROPERTY.forEach((element, index) => {
            /**
             * không cho qua -> bắt buộc kiểm
             */
            // this.logservice.Log(`Property: ${element.property}`);
            if (!element.skip) {
                let error = this.checkValue(object, element, index, element.name);
                if (error) {
                    // console.log(error);
                    if (row["error"]) {
                        row["error"] += `, ${error}`;
                    } else {
                        row["error"] = error;
                    }
                }
            }
            row[element.property] = (object[index] && object[index] !== null) ? object[index].trim() : null;
        })
        return row;
    }

    /**
     * Xem file /utils/###kebab###.validate.ts
     * Kiểm tra giá trị
     * Lấy tất cả thuộc tính từ hàm mapping lấy từ trong vòng lặp
     */
    checkValue = (object, element, index, name): string => {

        if (element.notNull) {
            //Kiểm tra rỗng
            if (!###Camel###Validation.validationNull(object[index])) {
                return ###Camel###Validation.thongBaoSaiDinhDang("null", name)
            }
            //Kiểm tra có phải số không
            //if (!###Camel###Validation.validationNumber(object[index])) {
            //    return ###Camel###Validation.thongBaoSaiDinhDang("number", name)
            //}
        }
        // if (element.property === 'ngay_sinh') {
        //   if (!###Camel###Validation.validationNgaySinh(object[index])) {
        //     return ###Camel###Validation.thongBaoSaiDinhDang(element.property);
        //   }
        // }
        // if (element.property === 'dien_thoai') {
        //   if (!###Camel###Validation.validationPhoneNumber(object[index])) {
        //     return ###Camel###Validation.thongBaoSaiDinhDang(element.property)
        //   }
        // }
        // if (element.property === 'email') {
        //   if (!###Camel###Validation.validationEmail(object[index])) {
        //     return ###Camel###Validation.thongBaoSaiDinhDang(element.property);
        //   }
        // }
        return null;
    }

    /**
     * Mở file upload
     */
    openFileUpload() {
        this.fileUpload.nativeElement.click()
    }

    goToList = () => {
        this.router.navigate(['/admin/###kebab###']);
    }

    /**
    * Hàm download dữ liệu mẫu
    */
    downloadExample() {
        this.service.buildExcel({data: Config.ImportExcel.DATA_SAMPLE })
            .then(result => {
                let data = new Blob([this.convert(result.replace(/"/g, ''))]);
                saveAs(data, Config.ImportExcel.FILE_NAME)
            })
            .catch(error => {
                this.toastr.error('Có lỗi xảy ra . Không thể download file');
            })
    }

    /**
     * xuất file nội dung lỗi
     */
    exportError = () => {
        this.service.buildExcel({data: this.exportErrorData})
            .then(result => {
                let data = new Blob([this.convert(result.replace(/"/g, ''))]);
                saveAs(data, Config.ImportExcel.FILE_NAME_ERROR)
            })
            .catch(error => {
                console.log(error);
                this.toastr.error('Có lỗi xảy ra . Không thể export file');
            })
    }

    /**
   * Decode base64 để download
   */
    convert(base64) {
        let binary_string = window.atob(base64);
        let len = binary_string.length;
        let bytes = new Uint8Array(len);
        for (let i = 0; i < len; i++) {
            bytes[i] = binary_string.charCodeAt(i);
        }
        return bytes.buffer;
    }

}
