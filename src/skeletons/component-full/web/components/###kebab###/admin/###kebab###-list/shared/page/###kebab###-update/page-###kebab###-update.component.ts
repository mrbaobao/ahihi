import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {Config} from 'app/components/###kebab###/config';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {ToastsManager} from 'ng2-toastr';

import {###Camel###} from "app/components/###kebab###/model/###kebab###.model";
import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';

/*
* @author ###author###
*/
@Component({
    selector: 'page-###kebab###-update',
    templateUrl: './page-###kebab###-update.component.html',
    styleUrls: ['./page-###kebab###-update.component.scss']
})
export class Page###Camel###UpdateComponent implements OnInit {

    private loaded = false;
    @@@if(hasStatus)@@@
    private select###StatusCamel### = Config.###StatusCamel###Map;
    @@@/if@@@
    
    private data: ###Camel### = new ###Camel###();
    //Block form
    private isBlocking: boolean = false;
    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private service: ###Camel###Service,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = +params['id'];
            console.log(id);
            this.service.getOne(id).then(value => {
                console.log(value);
                this.data.###id_snake### = value.###id_snake###;
                @@@if(hasStatus)@@@
                this.data.###status_snake### = value.###status_snake######status_value_suffix###;
                @@@/if@@@
                @@@if(!hasStatus)@@@
                this.data.###sample_snake### = value.###sample_snake###;
                @@@/if@@@
                this.loaded = true;
            });
        })
    }
    update = () => {
        if(this.isBlocking){
            return;
        }
        this.isBlocking = true;
        
        let body = {
            @@@if(hasStatus)@@@
                ###status_snake###: ###status_value_prefix###this.data.###status_snake###,
            @@@/if@@@
            @@@if(!hasStatus)@@@
            ###sample_snake###: this.data.###sample_snake###,
            @@@/if@@@
            ###id_snake###: +this.data.###id_snake###
        }
        this.service.update(body)
            .then(value => {
                this.toastr.success("Cập nhật thành công");
                this.router.navigate(['/admin/###kebab###']);
                this.isBlocking = false;
            }).catch(err => {
                this.toastr.error("Cập nhật thất bại");
                this.isBlocking = false;
            })

    }

    back() {
        this.router.navigate(['/admin/###kebab###']);
    }
}
