import {Component, OnInit, Output, EventEmitter, ViewContainerRef, ViewChild} from '@angular/core';
import {Config} from 'app/components/###kebab###/config';
import {ToastsManager} from 'ng2-toastr';
import {ModalDirective} from 'ngx-bootstrap';

import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';
import {###Camel###} from 'app/components/###kebab###/model/###kebab###.model';

/*
* @author ###author###
*/
@Component({
    selector: 'modal-###kebab###-create',
    templateUrl: './modal-###kebab###-create.component.html',
    styleUrls: ['./modal-###kebab###-create.component.scss']
})
export class Modal###Camel###CreateComponent {
    @ViewChild('lgModal') public _modal: ModalDirective;
    @Output() success = new EventEmitter()
    private loaded = false;
    @@@if(hasStatus)@@@
    private select###StatusCamel### = Config.###StatusCamel###Map;
    @@@/if@@@
    private data: ###Camel### = new ###Camel###();
    //Block form
    private isBlocking: boolean = false;

    constructor(private service: ###Camel###Service,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager
    ) {}

    show() {
        this.loaded = true;
        @@@if(hasStatus)@@@
        this.data.###status_snake### = 1;
        @@@/if@@@
        this._modal.show();
    }

    close = () => {
        this._modal.hide();
        setTimeout(() => {
            this.loaded = false;
        }, 50);
    }

    create = () => {
        if(this.isBlocking){
            return;
        }
        this.isBlocking = true;
        @@@if(hasStatus)@@@
        let body = {
            ###status_snake###: ###status_value_prefix###this.data.###status_snake###
        }
        @@@/if@@@
        @@@if(!hasStatus)@@@
        let body = {
            ###sample_snake###: this.data.###sample_snake###
        }
        @@@/if@@@
        this.service.create(body)
            .then(value => {
                this.toastr.success("Thêm thành công");
                this.success.emit();
                this._modal.hide();
                this.isBlocking = false;
            }).catch(err => {
                this.toastr.error("Thêm thất bại");
                this.isBlocking = false;
            })

    }


}
