import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ActivatedRoute, Router, Params} from '@angular/router';
import {Config} from 'app/components/###kebab###/config';
import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';
import {###Camel###} from 'app/components/###kebab###/model/###kebab###.model';

/*
* @author ###author###
*/
@Component({
    selector: 'page-###kebab###-detail',
    templateUrl: './page-###kebab###-detail.component.html',
    styleUrls: ['./page-###kebab###-detail.component.scss']
})
export class Page###Camel###DetailComponent implements OnInit {

    private data: ###Camel### = new ###Camel###();
    /**
     * tạo loading khi load dữ liệu
     */
    private loaded: boolean = false;
    @@@if(hasStatus)@@@
    private ###StatusCamel###MapThuan = Config.###StatusCamel###MapThuan;
    @@@/if@@@
    constructor(
        private router: Router,
        private vcr: ViewContainerRef,
        private activatedRoute: ActivatedRoute,
        private service: ###Camel###Service) {}

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            let id = +params['id'];
            this.service.getOne(id).then(value => {

                this.data.###id_snake### = value.###id_snake###;
                @@@if(hasStatus)@@@
                this.data.###status_snake### = value.###status_snake######status_value_suffix###;
                @@@/if@@@
                @@@if(!hasStatus)@@@
                this.data.###sample_snake### = value.###sample_snake###;
                @@@/if@@@
                
                this.loaded = true;
            });
        });
    }

    back() {
        this.router.navigate(['/admin/###kebab###']);
    }

}
