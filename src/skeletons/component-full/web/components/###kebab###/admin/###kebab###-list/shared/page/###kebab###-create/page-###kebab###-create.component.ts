import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ToastsManager} from 'ng2-toastr';
import {Router} from '@angular/router';

import {Config} from 'app/components/###kebab###/config';
import {###Camel###} from 'app/components/###kebab###/model/###kebab###.model';
import {###Camel###Service} from 'app/components/###kebab###/service/###kebab###.service';

/*
* @author ###author###
*/

@Component({
    selector: 'page-###kebab###-create',
    templateUrl: './page-###kebab###-create.component.html',
    styleUrls: ['./page-###kebab###-create.component.scss']
})
export class Page###Camel###CreateComponent implements OnInit {

    private loaded = false;
    @@@if(hasStatus)@@@
    private select###StatusCamel### = Config.###StatusCamel###Map;
    @@@/if@@@
    private data: ###Camel### = new ###Camel###();

    //Block form
    private isBlocking: boolean = false;
    constructor(
        private router: Router,
        private service: ###Camel###Service,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager
    ) {
        this.toastr.setRootViewContainerRef(vcr);
    }

    ngOnInit() {
        this.loaded = true;
        @@@if(hasStatus)@@@
        this.data.###status_snake### = 0;
        @@@/if@@@
        
    }
    create = () => {
        if(this.isBlocking){
            return;
        }
        this.isBlocking = true;
        
        let body = {
            @@@if(hasStatus)@@@
            ###status_snake###: ###status_value_prefix###this.data.###status_snake###,
            @@@/if@@@
            @@@if(!hasStatus)@@@
            ###sample_snake###: this.data.###sample_snake###,
            @@@/if@@@
        }
        this.service.create(body)
            .then(value => {
                this.toastr.success("Thêm thành công");
                this.router.navigate(['/admin/###kebab###']);
                this.isBlocking = false;
            }).catch(err => {
                this.toastr.error("Thêm thất bại");
                this.isBlocking = false;
            })

    }

    back() {
        this.router.navigate(['/admin/###kebab###']);
    }

}
