import {Component, Output, EventEmitter, OnInit} from '@angular/core';

import {IAddOnComponent} from 'app/shared/add-on.interface';
import {ManageStateService} from 'app/shared/manage-state.service';
/*
* @author ###author###
*/
@Component({
    selector: 'com-search-bar-###kebab###',
    template: `
        <div class="input-group">
            <input [(ngModel)]="_searchValue" type="text" class="form-control with-danger-addon" placeholder="Tìm kiếm theo ID..." (keydown.enter)="search()">
            <span class="input-group-btn">
                <button class="btn btn-danger" type="button" (click)="search()">Tìm</button>
            </span>
        </div>
    `
})
export class ComSearchBar###Camel###Component implements IAddOnComponent, OnInit {
    @Output() searchTrigger = new EventEmitter();
    private _searchValue = "";
    private _keyValue = '###kebab###:search';

    constructor(private manageStateService: ManageStateService) {
        this.loadState();
    }

    ngOnInit() {
    }

    getState() {
        return {name: '###id_snake###', value: this._searchValue};
    }

    private search() {
        this.saveState();
        this.searchTrigger.emit(this._searchValue);
    }

    private saveState() {
        this.manageStateService.save(this._keyValue, this._searchValue);
    }

    private loadState() {
        this._searchValue = this.manageStateService.load(this._keyValue);
    }
}