import * as moment from 'moment'
/**
 * @author ###author###
 */
export class Config {
    @@@if(hasStatus)@@@
    //Danh sách ###status-field-label###
    public static readonly ###StatusCamel###Map = [
###status-id-text-list###
    ];
    public static readonly ###StatusCamel###MapNguoc = {
###status-id-text-list-reverse###
    };
    public static readonly ###StatusCamel###MapThuan = {
###status-id-text-list-json###
    };
    //Import
    public static readonly ImportExcel = {
        /**
        * Tên file mẫu import
        */
        FILE_NAME: `${moment(new Date()).format('YYYYMMDD')}Import###Camel###.xlsx`,
        /* 
        Tên file import bị lỗi
        */
        FILE_NAME_ERROR: `${moment(new Date()).format('YYYYMMDD')}Import###Camel###Error.xlsx`,
        /**
         * Dùng để check đúng mẫu excel không?
         */
        HEADER_COLUMN: ['###status_snake###'],
        /**
         * Dữ liệu mẫu
         */
        DATA_SAMPLE: [['###status_snake###'], ["###status_sample_value###"]],
        /* 
        Các trường dữ liệu tương ứng với cột trong file excel import, Lưu ý ghi đúng thứ tự từ trái qua phải
         Dùng để kiểm tra và hiển thị các dòng lỗi
         property tên thuộc tính của đối tượng
         isDate=true -> check Date
         notNull=true -> check có null không
         skip=true -> bỏ qua không check lỗi
        */
        VALIDATION_PROPERTY: [
            { property: '###status_snake###', name: '###status-field-label###', isDate: false, notNull: true, skip: false },
        ]
    };
    public static readonly ExportExcel = {
        /* 
        Tên file excel export
        */
        EXPORT_FILE_NAME: `${moment(new Date()).format('YYYYMMDD')}Export###Camel###.xlsx`,
        /* 
        Map từng thuộc tính thành cột trong excel
        property là tên thuộc tính đối tượng
        header là tên cột trong excel
        */
        EXPORT_MAP: [
            { property: "###id_snake###", header: "ID" },
            { property: "###status_snake###", header: "###status-field-label###" }
        ],
        ERROR: [
            {
                property: "###status_snake###", header: "###status-field-label###"
            },
            { property: "error", header: "Lỗi" },
        ]
    }
    @@@/if@@@
    @@@if(!hasStatus)@@@
    
    //Import
    public static readonly ImportExcel = {
        /**
        * Tên file mẫu import
        */
        FILE_NAME: `${moment(new Date()).format('YYYYMMDD')}Import###Camel###.xlsx`,
        /* 
        Tên file import bị lỗi
        */
        FILE_NAME_ERROR: `${moment(new Date()).format('YYYYMMDD')}Import###Camel###Error.xlsx`,
        /**
         * Dùng để check đúng mẫu excel không?
         */
        HEADER_COLUMN: ['###sample_snake###'],
        /**
         * Dữ liệu mẫu
         */
        DATA_SAMPLE: [['###sample_snake###'], ["###sample_value###"]],
        /* 
        Các trường dữ liệu tương ứng với cột trong file excel import, Lưu ý ghi đúng thứ tự từ trái qua phải
         Dùng để kiểm tra và hiển thị các dòng lỗi
         property tên thuộc tính của đối tượng
         isDate=true -> check Date
         notNull=true -> check có null không
         skip=true -> bỏ qua không check lỗi
        */
        VALIDATION_PROPERTY: [
            { property: '###sample_snake###', name: '###sample-field-label###', isDate: false, notNull: true, skip: false },
        ]
    };
    public static readonly ExportExcel = {
        /* 
        Tên file excel export
        */
        EXPORT_FILE_NAME: `${moment(new Date()).format('YYYYMMDD')}Export###Camel###.xlsx`,
        /* 
        Map từng thuộc tính thành cột trong excel
        property là tên thuộc tính đối tượng
        header là tên cột trong excel
        */
        EXPORT_MAP: [
            { property: "###id_snake###", header: "ID" },
            { property: "###sample_snake###", header: "###sample-field-label###" }
        ],
        ERROR: [
            {
                property: "###sample_snake###", header: "###sample-field-label###"
            },
            { property: "error", header: "Lỗi" },
        ]
    }
    @@@/if@@@
}