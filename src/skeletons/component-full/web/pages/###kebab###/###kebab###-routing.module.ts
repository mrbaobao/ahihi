import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ###Camel###Page } from './admin/###kebab###.page';

import { 
  Page###Camel###ListComponent,
  Page###Camel###DetailComponent,
  Page###Camel###CreateComponent,
  Page###Camel###UpdateComponent,
  Page###Camel###ImportComponent
} from 'app/components/###kebab###/index';


/*
* @author ###author###
*/
const routes: Routes = [
  {
    path: '',
    component: ###Camel###Page,
    children: [
      { path: '', redirectTo: 'quan-ly-###kebab###', pathMatch: 'full' },
      { path: 'quan-ly-###kebab###', component: Page###Camel###ListComponent },
      { path: 'chi-tiet-###kebab###/:id', component: Page###Camel###DetailComponent },
      { path: 'cap-nhat-###kebab###/:id', component: Page###Camel###UpdateComponent },
      { path: 'them-###kebab###', component: Page###Camel###CreateComponent },
      { path: 'import-###kebab###', component: Page###Camel###ImportComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ###Camel###RoutingModule { }
