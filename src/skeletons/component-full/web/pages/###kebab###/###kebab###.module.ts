import { NgModule } from '@angular/core';

import { ###Camel###Page } from './admin/###kebab###.page';

import { ###Camel###RoutingModule } from './###kebab###-routing.module';

import { ###Camel###Module as ###Camel###ComponentModule } from '../../components/###kebab###/###kebab###.module';
/*
*	@author ###author###
*/
@NgModule({
  imports: [###Camel###RoutingModule, ###Camel###ComponentModule],
  declarations: [###Camel###Page],
})
export class ###Camel###Module { }
