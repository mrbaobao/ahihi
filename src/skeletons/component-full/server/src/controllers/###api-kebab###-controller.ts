import * as express from 'express';
import * as pg from 'pg';
import * as path from 'path';

import { IBaseController } from "./base-controller";
import { ###Api###Repository } from '../repositories';
import { VARIABLES, ControllerUtil } from '../util';
import { ###Api### } from '../models';
/**
 * @author ###author###
 */
export class ###Api###Controller {
    private _###apiokman###Repo: ###Api###Repository;
    constructor() {
        this._###apiokman###Repo = new ###Api###Repository();
    }

    /**
    * @author ###author###
    * update 1 sự cố
    * api/baocao###apiokman###/update
    * {
    *  "note":"đang xử lý",
    *  @@@if(hasStatus)@@@"###status_snake###":1@@@/if@@@
    * }
    */
    update = (req: express.Request, res: express.Response): void => {
        req.body.user_update = req.user.id;
        if (!req.body.###id_snake### || !req.body.user_update) {
            res.status(400).json({
                error: {
                    error_type: VARIABLES.ErrorMessage.MISSING_PARAM,
                    message: VARIABLES.ErrorMessage.NEED_COLUMN_IN(['###id_snake###', 'user_update'], ['int', 'string'], 'body')
                }
            });
            return;
        }

        let promiseResult = this._###apiokman###Repo.update(req.body)
            .then(result => {
                return result.rowCount;
            });
        ControllerUtil.resovleResponse(req, res, promiseResult);
    }

    /**
     * @author ###author###
     * @description dùng để lấy danh sách sự cố hoặc tìm kiếm có phân trang
     * api/###apiokman###/search
     * {
     *  "keyword":"Thanh",
     *  "page":1,
     *  "per_page":15
     * }
     */
    search = (req: express.Request, res: express.Response): void => {
        let limit = req.body.per_page || 10;
        let offset;
        if (!req.body.page || req.body.page === 1) {
            offset = 0;
        } else {
            offset = limit * (req.body.page - 1);
        }
        let order = req.body.order || '###id_snake###+0';
        @@@if(hasStatus)@@@
        let ###status_snake### = req.body.###status_snake###;
        @@@/if@@@
        delete req.body.page;
        delete req.body.per_page;
        delete req.body.order;

        let promiseNumberOfRow = this._###apiokman###Repo.count(req.body@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@);
        let promiseResult = this._###apiokman###Repo.search(req.body, order, limit, offset@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@)
            .then(###apiokman###s => {
                return promiseNumberOfRow.then(numberOfRow => {
                    return { result: ###apiokman###s.rows, number_of_all_data: numberOfRow.rows[0] };
                })
            });

        ControllerUtil.resovleResponse(req, res, promiseResult);
    }

    /**
    * @author ###author###
    * api/###apiokman###/getone
    * {
    *  "###id_snake###":1
    * }
    */
    getOne = (req: express.Request, res: express.Response): void => {
        if (!req.query.###id_snake###) {
            res.status(400).json({
                error: {
                    error_type: VARIABLES.ErrorMessage.MISSING_PARAM,
                    message: VARIABLES.ErrorMessage.NEED_COLUMN_IN(['###id_snake###'], ['int'], 'body')
                }
            });
            return;
        }
        let promiseResult = this._###apiokman###Repo.getOne(req.query.###id_snake###)
            .then(result => {
                return { result: result.rows[0] };
            });
        ControllerUtil.resovleResponse(req, res, promiseResult);
    }

    /**
     * @author ###author###
     * thêm 1 row
     */
    insert = (req: express.Request, res: express.Response): void => {
        let promiseresult = this._###apiokman###Repo.insert(req.body)
            .then(value => {
                return { result: req.body };
            })
        ControllerUtil.resovleResponse(req, res, promiseresult);
    }

    /**
     * @author ###author###
     * xóa 1 hay nhiều dòng
     */
    delete = (req: express.Request, res: express.Response): void => {

        if (!req.body.###id_snake###s) {
            res.status(400).json({
                error: {
                    error_type: VARIABLES.ErrorMessage.MISSING_PARAM,
                    message: VARIABLES.ErrorMessage.NEED_COLUMN_IN(['###id_snake###s'], ['mảng int'], 'body')
                }
            });
            return;
        }

        let promiseResult = this._###apiokman###Repo.delete(req.body.###id_snake###s)
            .then(result => {
                return { result: result.rowCount };
            })

        ControllerUtil.resovleResponse(req, res, promiseResult);
    }


    /**
    * @author ###author###
    * xóa sự cố filter
    */
    deleteFilter = (req: express.Request, res: express.Response): void => {
        @@@if(hasStatus)@@@
        let ###status_snake### = req.body.###status_snake###;
        @@@/if@@@
        delete req.body.page;
        delete req.body.per_page;
        delete req.body.order;

        let promiseResult = this._###apiokman###Repo.deleteFilter(req.body@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@)
            .then(result => {
                return { result: result.rowCount };
            })

        ControllerUtil.resovleResponse(req, res, promiseResult);
    }


    /**
     * @author ###author###
     * lấy sự cố theo filter
     */
    getsFilter = (req: express.Request, res: express.Response): void => {
        @@@if(hasStatus)@@@
        let ###status_snake### = req.body.###status_snake###;
        @@@/if@@@
        delete req.body.page;
        delete req.body.per_page;
        delete req.body.order;
        let order = req.body.order || '###id_snake###+0';
        let promiseResult = this._###apiokman###Repo.getsFilter(req.body, order@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@)
            .then(###apiokman###s => {
                return { result: ###apiokman###s.rows };
            });

        ControllerUtil.resovleResponse(req, res, promiseResult);
    }

    /**
      * lấy file mẫu
      */
    getsDuLieuMau = (req: express.Request, res: express.Response): void => {
		let filename = "###apiokman###mau.xlsx";
        let url = path.join(__dirname, '..', 'bin', 'excel', filename);
        res.download(url, filename);
    } 

}