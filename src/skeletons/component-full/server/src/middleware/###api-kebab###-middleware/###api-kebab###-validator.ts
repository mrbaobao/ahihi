import express = require('express');
import { ###Api###Schema } from './###api-kebab###-schema';
/**
 * @author ###author###
 */
/**
 * Middleware dùng cho validate toàn bộ NoiDung api
 */
export class ###Api###Validator {
    private _schema = new ###Api###Schema();
    validate = (req: express.Request, res: express.Response, next: express.NextFunction) => {
        req.check(this._schema.schema);
        req.getValidationResult().then(result => {
            if (result.isEmpty()) {
                next();
            } else {
                res.status(400).json(result.array());
            }
        });

    }
}