
import { VARIABLES } from '../../util';
import { ###Api###CurrentStatusEnum } from '../../models';
/**
 * @author ###author###
 */
export class ###Api###Schema {
    private _schema;

    constructor() {
        this._schema = {
###api-middleware-schema###
            'limit': {
                optional: true,
                isInt: {
                    options: { min: 1 }, errorMessage: VARIABLES.ValidatorMessage.IS_INT + ' và >= 0'
                }
            },
            'standard': {
                optional: true,
                isInStandardFormat: { errorMessage: VARIABLES.ValidatorMessage.IS_IN_STANDARD_FORMAT }
            },
            'offset': {
                optional: true,
                isInt: {
                    errorMessage: VARIABLES.ValidatorMessage.IS_INT
                }
            }
        };
    }

    get schema() {
        return this._schema;
    }
}