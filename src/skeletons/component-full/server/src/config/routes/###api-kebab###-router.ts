import express = require("express");
import { ###Api###Controller } from '../../controllers';
import { ###Api###Validator } from '../../middleware';

let router = express.Router();
/*
*   @author ###author###
*/
export class ###Api###Router {
    private _###apiokman###Ctr: ###Api###Controller;

    constructor() {
        this._###apiokman###Ctr = new ###Api###Controller();
    }

    get routes() {
        router.use(new ###Api###Validator().validate);
        router.post("/search", this._###apiokman###Ctr.search);
        router.get("/getone", this._###apiokman###Ctr.getOne);
        router.post("/create", this._###apiokman###Ctr.insert);
        router.post("/update", this._###apiokman###Ctr.update);
        router.post("/delete", this._###apiokman###Ctr.delete);
        router.post("/delete-by-filter", this._###apiokman###Ctr.deleteFilter);
        router.post("/gets-by-filter", this._###apiokman###Ctr.getsFilter);
        router.get("/get-du-lieu-mau", this._###apiokman###Ctr.getsDuLieuMau);

        return router;
    }
}

Object.seal(###Api###Router);