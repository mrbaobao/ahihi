
import * as validator from 'validator';
import * as pg from 'pg';


import { ###Api### } from '../models';
import { RepoUtil } from './util';
import { PgConnect } from './base';

interface QueryOption {
###api-query-option###
}
/**
 * @author ###author###
 */
export class ###Api###Repository {

    private pg: PgConnect;
    constructor() {
        this.pg = new PgConnect("###pgConnect###");
    }
	
    /*
     * @author ###author###
     * @description dùng để lấy danh sách hoặc tìm kiếm sự cố
     * 
     */
    public search(option, orderBy?: string, limit?: number, offset?: number@@@if(hasStatus)@@@, ###status_snake###?: Array<number>@@@/if@@@): Promise<pg.QueryResult> {
        //let params = [];
        let array_query = [];
		let where = RepoUtil.buildWhere({
			###id_snake###: +option.###id_snake###@@@if(hasStatus)@@@,
			###status_snake###: ###status_snake###@@@/if@@@
		});
		
        let strQuery = `
			SELECT
				###id_snake###@@@if(hasStatus)@@@,###status_snake###@@@/if@@@@@@if(!hasStatus)@@@,###sample_snake###@@@/if@@@
			FROM ###db_table_name_snake### 
			WHERE ${where.str}
		`;
		let params = where.params;
        if (orderBy) {
            strQuery += ' ' + RepoUtil.convertQueryForOrderBy(orderBy);
        }
        if (limit) {
            strQuery += `
			LIMIT $${params.length + 1} 
			`;
            params.push(limit);
        }
        if (offset) {
            strQuery += `
			OFFSET $${params.length + 1} 
			`;
            params.push(offset);
        }
        return this.pg.runAQuery(strQuery, params)
            .catch(error => {
                console.log(error);
                error['strQuery'] = strQuery;
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * đếm số lượng record trả về từ hàm search nhưng không phân trang
     */
    public count(option@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@): Promise<pg.QueryResult> {
        let array_query = [];
		let where = RepoUtil.buildWhere({
			###id_snake###: +option.###id_snake###@@@if(hasStatus)@@@,
			###status_snake###: ###status_snake###@@@/if@@@
		});
        let strQuery = `
			SELECT COUNT(*) 
			FROM ###db_table_name_snake### 
			WHERE ${where.str}
		`;
		let params = where.params;
        //Query
        return this.pg.runAQuery(strQuery, params);
    }

    /**
    * @author ###author###
    * lấy 1 sự cố
    */
    public getOne = (id: number): Promise<pg.QueryResult> => {
        let strQuery = `
			SELECT
                @@@if(hasStatus)@@@
				###status_snake###,
                @@@/if@@@
                @@@if(!hasStatus)@@@
                ###sample_snake###,
                @@@/if@@@
                ###id_snake###
			FROM ###db_table_name_snake### 
			WHERE ###id_snake### = $1
		`;
        return this.pg.runAQuery(strQuery, [id])
            .catch(error => {
                error['strQuery'] = strQuery;
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * thêm 1 
     */
    public insert(option: ###Api###): Promise<pg.QueryResult> {
        let strQuery;
        strQuery = `
            INSERT INTO ###db_table_name_snake###(
                time_create,time_update,
                @@@if(hasStatus)@@@
                ###status_snake###
                @@@/if@@@
                @@@if(!hasStatus)@@@
                ###sample_snake###
                @@@/if@@@
			)
            VALUES(
                CURRENT_TIMESTAMP,
                CURRENT_TIMESTAMP,
                $1
			);
        `;
        return this.pg.runAQuery(strQuery,
            [
                @@@if(hasStatus)@@@
                option.###status_snake###
                @@@/if@@@
                @@@if(!hasStatus)@@@
                option.###sample_snake###
                @@@/if@@@
            ])
            .catch(error => {
                error['strQuery'] = strQuery;
                error['value'] = option;
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * xóa theo bộ lọc
     */
    public deleteFilter = (option: ###Api###@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@): Promise<pg.QueryResult> => {
        let array_query = [];
		let where = RepoUtil.buildWhere({
			###id_snake###: +option.###id_snake###@@@if(hasStatus)@@@,
			###status_snake###: ###status_snake###@@@/if@@@
		});
        let strQuery = `
			SELECT
				###id_snake###
			FROM ###db_table_name_snake### 
			WHERE ${where.str}
		`;
		let params = where.params;
        let query_delete = `
			DELETE
			FROM ###db_table_name_snake###
			WHERE ###id_snake### IN ( ${strQuery} )
		`;
        return this.pg.runAQuery(query_delete, params)
            .catch(error => {
                error['strQuery'] = strQuery;
                error['value'] = option;
                return Promise.reject(error);
            });
    }

    /**
     * @author ###author###
     * xóa 1 hay nhiều sự cố
     */
    public delete(ids: number[]): Promise<pg.QueryResult> {
        let strQuery;
        strQuery = `
			DELETE
			FROM ###db_table_name_snake###
			WHERE ###id_snake### = ANY($1::int[])
		`;
        return this.pg.runAQuery(strQuery, [ids])
            .catch(error => {
                error['strQuery'] = strQuery;
                return Promise.reject(error);
            });

    }

    /**
     * @author ###author###
     * lấy theo filter ko phân trang
     */
    public getsFilter(option: ###Api###, orderBy: string@@@if(hasStatus)@@@, ###status_snake###@@@/if@@@): Promise<pg.QueryResult> {
        let array_query = [];
		let where = RepoUtil.buildWhere({
			###id_snake###: +option.###id_snake###@@@if(hasStatus)@@@,
			###status_snake###: ###status_snake###@@@/if@@@
		});
        let strQuery = `
			SELECT ###id_snake###
                @@@if(hasStatus)@@@, ###status_snake###@@@/if@@@
                @@@if(!hasStatus)@@@, ###sample_snake###@@@/if@@@
			FROM ###db_table_name_snake###
			WHERE ${where.str}
		`;
		let params = where.params;
        if (orderBy) {
            strQuery += ' ' + RepoUtil.convertQueryForOrderBy(orderBy);
        }
        return this.pg.runAQuery(strQuery, params)
            .catch(error => {
                error['strQuery'] = strQuery;
                return Promise.reject(error);
            });
    }

    /**
    * @author ###author###
    * update 
    */
    public update(option: QueryOption): Promise<pg.QueryResult> {
		/*
			Mọi field trong update phải tường minh!!!
			Không nên dùng loại trừ.
		*/

		let params = [
            @@@if(hasStatus)@@@
            option.###status_snake###,
            @@@/if@@@
            @@@if(!hasStatus)@@@
            option.###sample_snake###,
            @@@/if@@@
            option.###id_snake###
        ];

        let strQuery = `
			UPDATE ###db_table_name_snake###
			SET
				@@@if(hasStatus)@@@
                ###status_snake### = $1,
                @@@/if@@@
                @@@if(!hasStatus)@@@
                ###sample_snake### = $1,
                @@@/if@@@
				time_update = CURRENT_TIMESTAMP
            WHERE ###id_snake### = $2
		`;
		//Query
        return this.pg.runAQuery(strQuery, params)
            .catch(error => {
                error['strQuery'] = strQuery;
                return Promise.reject(error);
            });
    }

}