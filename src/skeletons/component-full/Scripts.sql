﻿-- chạy bên database chứa table đang sinh code 
-- tạo 5 trường mặc định vào bảng dữ liệu
ALTER TABLE ###db_table_name_snake###
ADD time_create timestamp(6) without time zone;
ALTER TABLE ###db_table_name_snake###
ADD time_update timestamp(6) without time zone;
ALTER TABLE ###db_table_name_snake###
ADD user_update character varying(500);
ALTER TABLE ###db_table_name_snake###
ADD user_create character varying(500);
ALTER TABLE ###db_table_name_snake###
ADD current_status character varying(255);

-- phân quyền api
-- chạy bên database: hutech
-- ví dụ: localhost:3100/api/###kebab###/search
-- thêm controller
-- app_id nhớ là phải sửa lại theo app nào đó
-- chạy bên database hutech
insert into r_controller(controller_name,app_id,time_create,time_update) values('###kebab###',8,current_timestamp,current_timestamp);

-- thêm action 
insert into r_action(action_name,controller_id,public)
values('search',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('getone',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('create',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('update',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('delete',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('delete-by-filter',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('gets-by-filter',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);
insert into r_action(action_name,controller_id,public)
values('get-du-lieu-mau',(select controller_id from r_controller where controller_name='###kebab###' and app_id=8),false);

-- sau khi xong nhớ phân quyền action này vào role
 