
import {###Camel###} from "../../model/###kebab###.model";
import {###Camel###Service} from "app/components/###kebab###/service/###kebab###.service";

import {Component, OnInit, ViewChild, ViewContainerRef, EventEmitter, Output} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";
import {ToastsManager} from 'ng2-toastr/ng2-toastr';


@Component({
    selector: 'app-###snakeAction###',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class ###CamelAction###Component {
    @ViewChild('lgModal') public _modal: ModalDirective;
    /**
     * tạo loading khi load dữ liệu
     */
    private loaded: boolean = false;

    private data: ###Camel###;

    @Output() success = new EventEmitter()

    constructor(
        private service: ###Camel###Service,
        private vcr: ViewContainerRef,
        private toastr: ToastsManager) {
        this.toastr.setRootViewContainerRef(vcr);
    }
    //Hiển thị
    show = (value) => {
        //New Model
        this.data = new ###Camel###();
        //Gán dữ liệu
        this.data.time_create = value.time_create;
        //Loaded
        this.loaded = true;
        this._modal.show();
    }
    //Đóng
    close = () => {
        this._modal.hide();
    }
    /*
     * Update
     */
    update = () => {
        //Data
        let body = {
            su_co_id: this.data.id,
        }
        //Call Service
        this.service.update(body)
            .then(value => {
                this.toastr.success("Cập nhật thành công!");
                this.success.emit();
                this._modal.hide();
            }).catch(err => {
                this.toastr.error("Cập nhật thất bại");
            })
    }

}
