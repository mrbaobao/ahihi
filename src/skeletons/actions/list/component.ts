
import {###Camel###AdminDetailComponent} from "app/components/###kebab###/admin/detail/###kebab###-admin-detail.component";
import {###Camel###AdminSearchComponent} from "app/components/###kebab###/admin/search/###kebab###-admin-search.component";
//Service
import {###Camel###Service} from "app/components/###kebab###/service/###kebab###.service";
import {###Camel###} from "app/components/###kebab###/model/###kebab###.model";
/**
 * Import Libraries
 */
import {Component, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {ModalDirective} from 'ngx-bootstrap';
import {IAddOnComponent} from '../../../../shared/add-on.interface';
import * as _ from 'lodash';
/**
 * Import Module bên ngoài Component này
 */
import {ManageStateService} from '../../../../shared/manage-state.service';
import {PagingComponent} from "app/shared/paging/paging.component";


@Component({
    selector: 'app-###snakeAction###',
    templateUrl: 'template.html',
    styleUrls: ['style.scss']
})
export class ###CamelAction###Component implements OnInit {
    /**
     * Private
     */
    private _data: Promise<###Camel###[]>;
    private _total: number = 0;
    private _itemsPerPage: number;
    private _currentPage: number;
    private _keyValue = '###snakeAction###:';
    private _isLoaded = false;
    private _selectedRow: Number;
    private _addOnComponents: Array<IAddOnComponent> = [];
    /**
     * ViewChild
     */
    @ViewChild(###Camel###AdminSearchComponent) private actionSearch: ###Camel###AdminSearchComponent;
    @ViewChild(###Camel###AdminDetailComponent) private actionDetail: ###Camel###AdminDetailComponent;
    
    @ViewChild('errorModal') errorModal;
    @ViewChild('pbmodal') pbModal;
    @ViewChild(PagingComponent) private _pagingComponent: PagingComponent;

    constructor(
        private service: ###Camel###Service,
        private manageStateService: ManageStateService,
        private router: Router
    ) {}

    ngOnInit() {
        this._addOnComponents = [
            this.actionSearch,
            this._pagingComponent
        ];
        this.loadState();
        this._data = this.getData().then(result => {
            // console.log(result);
            this._isLoaded = true;
            this._total = result.length;
            return result.data;
        });
    }
    private identify(index, item: ###Camel###) {
        return item.id;
    }

    private calculateAlphaNumber(): number {
        return (this._currentPage - 1) * this._itemsPerPage;
    }

    private getData(): Promise<any> {
        this._isLoaded = false;
        const body: any = {};
        this._selectedRow = -1;
        this._addOnComponents.forEach(ac => {
            const state = ac.getState();
            if (!_.isNil(state.value) && !_.isEmpty(state.value) && state.name !== 'page') {
                body[state.name] = state.value;
            }
            if (state.name === 'page' && state.value) {
                this._currentPage = state.value;
                this.saveState();
            }
        });
        return this.service.search(body, this._currentPage, this._itemsPerPage).then((result) => {
            this._isLoaded = true;
            return result;
        })
            .catch(error => {
                console.log(error);
                const message = `
                    Hiện tại không kết nối được tới server, xin hãy kiểm tra thiết bị có kết nối internet hay không.
                    Nếu có internet mà lỗi này vẫn hiện xin hãy báo lại với chúng tôi`;
                const title = 'Error';
                const buttonBackOption = {
                    text: 'Trở lại dashboard',
                    fn: this.goToDashBoard
                }
                this.errorModal.show(title, message, buttonBackOption);
                return {};
            });

    }
    private goToDashBoard = () => {
        this.router.navigate(['/admin/dashboard']);
    }
    private updatePerPage() {
        this.saveState();
        this.stateChange();
    }

    private saveState() {
        this.manageStateService.save(this._keyValue + 'perPage', this._itemsPerPage);
        this.manageStateService.save(this._keyValue + 'currentPage', this._currentPage);
    }

    private loadState() {
        this._itemsPerPage = this.manageStateService.load(this._keyValue + 'perPage') || 10;
        this._currentPage = this.manageStateService.load(this._keyValue + 'currentPage') || 1;
    }

    private stateChange() {
        this._data = this.getData().then(result => {
            // console.log(result);
            this._total = result.length;
            return result.data;
        });
    }

    /**
       * chọn 1 dòng
       */
    private selectRow(index) {
        this._selectedRow = index;
    }

    /**
     * xem detail 1 sự cố
     */
    openDetail = (value) => {
        this.actionDetail.show(value)
    }

}
