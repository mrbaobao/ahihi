import {###data###} from "app/util/variable";

import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {IAddOnComponent} from 'app/shared/add-on.interface';
import {ManageStateService} from 'app/shared/manage-state.service';


@Component({
    selector: '###snakeAction###',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class ###CamelAction###Component implements OnInit, IAddOnComponent {
    private _data: Array<any> = [];
    private _options;
    private _statusValue;
    private _keyValue = 'current_status';
    @Output() valueUpdated = new EventEmitter();

    constructor(private manageStateService: ManageStateService) {
        this.loadState();
    }

    ngOnInit() {
        this._options = {
            multiple: true,
            closeOnSelect: false,
            allowClear: true,
            placeholder: "",
            dropdownAutoWidth: true,
            width: '200px',
            height: '20px'
        }
        this._data = ###data###;
    }

    public getState() {
        return {name: "###nameInput###", value: this._statusValue};
    }

    private changed(e) {
        if (this._statusValue === e.value) {
            return;
        }
        this._statusValue = e.value;
        this.saveState();
        this.valueUpdated.emit(e.value);
    }

    private saveState() {
        this.manageStateService.save(this._keyValue, this._statusValue);
    }

    private loadState() {
        this._statusValue = this.manageStateService.load(this._keyValue);
    }

}
