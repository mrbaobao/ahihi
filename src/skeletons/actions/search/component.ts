import {Component, Output, EventEmitter, OnInit} from '@angular/core';
import {IAddOnComponent} from "app/shared/add-on.interface";
import {ManageStateService} from "app/shared/manage-state.service";

@Component({
    selector: 'search-bar-###snakeAction###',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class ###CamelAction###Component implements IAddOnComponent, OnInit {
    @Output() searchTrigger = new EventEmitter();
    private _searchValue = "";
    private _keyValue = '###snakeAction###-search-bar';

    constructor(private manageStateService: ManageStateService) {
        this.loadState();
    }

    ngOnInit() {
    }

    getState() {
        return {name: 'keyword', value: this._searchValue};
    }

    private search() {
        this.saveState();
        this.searchTrigger.emit(this._searchValue);
    }

    private saveState() {
        this.manageStateService.save(this._keyValue, this._searchValue);
    }

    private loadState() {
        this._searchValue = this.manageStateService.load(this._keyValue);
    }
}