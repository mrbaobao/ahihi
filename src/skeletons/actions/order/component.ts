import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { Select2OptionData } from 'ng2-select2';
import { IAddOnComponent } from 'app/shared/add-on.interface';
import { ManageStateService } from 'app/shared/manage-state.service';

@Component({
    selector: 'order-by',
    templateUrl: './template.html',
    styleUrls: ['./style.scss']
})
export class ###CamelAction###Component implements OnInit, IAddOnComponent {
    private data: Array<Select2OptionData>;
    @Output() valueUpdated = new EventEmitter();
    private _options: Select2Options;
    private _orderValue;
    private _defaultValue;
    private _keySave = '###keyInput###';

    constructor(private manageStateService: ManageStateService) {
        this.loadState();
    }

    ngOnInit() {
        this.data = [
            {
                id: 'ID',
                text: 'Sắp xếp',
                children: [
                    { id: 'so_hieu+0', text: 'Số hiệu tăng dần' },
                    { id: 'ma_sinh_vien+0', text: 'Mã sinh viên tăng dần' },
                    { id: 'ho_ten+0', text: 'Sắp xếp theo họ tên' },
                    { id: 'trang_thai_so_sanh+1', text: 'Chứng chỉ sai thông tin lên đầu' }
                ]
            }
        ]

        this._options = {
            multiple: true,
            closeOnSelect: false,
            allowClear: true,
            placeholder: 'sắp xếp theo',
            dropdownAutoWidth: true,
            width: 'auto'
        }
    }

    getState() {
        return { name: 'order', value: this._orderValue };
    }

    private saveState() {
        let state = {
            value: this._orderValue,
            default: this._defaultValue
        };
        this.manageStateService.save(this._keySave, state);
    }

    private loadState() {
        let state = this.manageStateService.load(this._keySave);
        if (state) {
            this._orderValue = state.value;
            this._defaultValue = state.default;
        }

    }

    private changed(e) {
        if (e.value) {
            let order = e.value.join(',');
            this._orderValue = order;
            this._defaultValue = e.value;
            this.saveState();
            this.valueUpdated.emit(order);
        } else {
            this.valueUpdated.emit(null);
            this._orderValue = null;
            this._defaultValue = null;
            this.saveState();
        }

    }


}