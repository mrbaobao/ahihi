/**
 * Import class nội bộ Component này
 */
import {###Camel###} from "../model/###kebab###.model";
/**
 * Import Angular & Libraries
 */
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
/**
 * Import class bên ngoài Component này
 */
import {LogService} from "app/shared/logger";
import {UrlVariable} from "app/util/variable";


@Injectable()
export class ###Camel###Service {
    //URL API
    private apiUrl: string = UrlVariable.URL_API_###Camel### + "/api/###kebab###/";
    //Constructor
    constructor(private http: Http, private logger: LogService) {}
    /**
     * @author 
     * @description dùng để update 1 sự cố
     */
    public update(body: any): Promise<any> {
        return this.http.post(this.apiUrl + "update", body)
            .toPromise()
            .then(value => value.json())
            .catch(err => err.json());
    }
    /**
     * @author 
     * @description lấy danh sách sự cố hoặc tìm kiếm
     */
    public search(searchBody: any, page: number, perPage: number
    ): Promise<{data: ###Camel###[], length: number}> {
        //Tạo body
        let body = this.getBody(searchBody, page, perPage);
        //Request server
        return this.http.post(this.apiUrl + "search", body)
            .toPromise()
            .then(response => {
                return response.json();
            })
            .then(jsonData => {
                return {
                    data: jsonData.result,
                    length: jsonData.number_of_all_data.count
                }
            })
            .then(jsonData => {
                return this.parseData(jsonData);
            });
    }
    /**
     * Chuyển dữ liệu search từ dạng JSON thô sang dữ liệu đúng của Component
     */
    protected parseData(jsonData: any): any{
        let outPut: ###Camel###[] = [];
        jsonData.data.forEach((rowData: any) => {
            outPut.push(<###Camel###>rowData);
        })
        jsonData.data = outPut;
        return jsonData;
    }
    
    /**
     * Tạo body cho http post
     */
    protected getBody(searchBody: any, page: number, perPage: number): any {
        let body: any = Object.assign({}, searchBody);
        body['per_page'] = perPage;
        body['page'] = page;
        return body;
    }
}

