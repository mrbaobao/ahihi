/**
 * Import Libraries
 */
import {NgModule} from '@angular/core';
import {RouterModule} from "@angular/router";
/**
 * Import các module shared dùng cho Component này
 */
import {SharedModule} from "app/shared/shared.module";
/**
 * Khai báo Modules
 */
@NgModule({
    imports: [
        SharedModule,
        RouterModule],
    declarations: [
        
    ],
    providers: [
    ]
})
/**
 * Module ###Camel###
 */
export class ###Camel###Module {}
