"use strict";

var name = {
    camel: "###Camel###",
    kebab: "###kebab###",
    kebabAction: "###kebabAction###",
    camelAction: "###CamelAction###"
};
exports.name = name;