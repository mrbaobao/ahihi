/* global process, __dirname */

"use strict";

/**
 * Import Config
 */
var Config = require("../config/Config");

var skeletons = "";
var components = "";
var tmp = "";
var templates = "";
/**
 * Path of core folder
 */
var path = require('path');
var arrCorePath = path.resolve(__dirname).split(/\\|\//);
arrCorePath.splice(-1);
arrCorePath.splice(-1);
var root = arrCorePath.join("/");
var clientRoot = "./";
/**
 * Check config
 */
if (Config.isProduction) {
    /**
     * Production paths
     */
    skeletons = root + "/src/skeletons/";
    components = "./src/app/components/";
    templates = "./templates/";
    tmp = root + "/tmp/";
} else
{
    /**
     * Dev paths
     */
    tmp = "./tmp/";
    skeletons = "../src/skeletons/";
    components = "../src/components/";
    templates = "./templates/";
}
/**
 * Exports
 */
exports.root = root;
exports.clientRoot = clientRoot;

exports.skeletons = skeletons;
exports.components = components;
exports.tmp = tmp;
exports.templates = templates;