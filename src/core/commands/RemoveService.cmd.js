/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
var removeImportFromModule = function(componentName, serviceName){
    var pathFileModule = Path.components + componentName + "/"+ componentName +".module.ts";
    var nameService = String.kebabToCamelBigHead(serviceName) + "Service";
    //Import
    var importLine = "import { " + nameService + " } from './service/" + serviceName + ".service' ;";
    File.removeLine(importLine, pathFileModule);
    //Remove from providers
    var strProvider = nameService + " ,";
    File.removeString(strProvider, pathFileModule);
    //In case declare service in provider without comma (,)
    File.removeString(nameService, pathFileModule);
};
var RemoveService = function (componentName, serviceName) {
    var pathComponent = Path.components + componentName + "/";
    if (!File.isDir(pathComponent))
    {
        Log.push("Component " + componentName + " is not existed.");
        return;
    }
    var pathFileService = pathComponent + "service/" + serviceName + ".service.ts";
    if (!File.isFile(pathFileService))
    {
        Log.push("Service " + componentName + "/" + serviceName + " is not existed.");
    } else {
        File.deleteFile(pathFileService);
    }
    var pathFileModel = pathComponent + "model/" + serviceName + ".model.ts";
    if (!File.isFile(pathFileModel))
    {
        Log.push("Model " + componentName + "/" + serviceName + " is not existed.");
    } else {
        File.deleteFile(pathFileModel);
    }
    //Remove from module
    removeImportFromModule(componentName, serviceName);
    //Log
    Log.push("Remove service(" + componentName + "/" + serviceName + ") success!");
};
/**
 * Exports
 */
exports.run = RemoveService;