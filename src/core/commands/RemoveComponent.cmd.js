/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");

var RemoveComponent = function (name) {
    var pathComponent = Path.components + name;
    if(!File.isDir(pathComponent))
    {
        Log.push("Component " + name + " is not existed.");
        return;
    }
    //Create tmp
    File.makeDir(Path.tmp);
    File.makeDir(Path.tmp + "components/");
    //Delete Component: Move to tmp dir
    File.moveDir(pathComponent, Path.tmp + "components/" + name);
    //Log
    Log.push("Remove component(" + name + ") success!");
    Log.push("Don't worry! The component can restore by command: ahh restore component " + name + ".");
};
/**
 * Exports
 */
exports.run = RemoveComponent;