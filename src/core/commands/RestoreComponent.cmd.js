/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");

var RestoreComponent = function (name) {
    //Delete Component: Move to tmp dir
    if (File.isDir(Path.tmp + "components/" + name)) {
        if (!File.isDir(Path.components + name)) {
            File.moveDir(Path.tmp + "components/" + name, Path.components + name);
            //Log
            Log.push("Restore component(" + name + ") success!");
        } else {
            ErrorHandler.push("Component " + name + " is already existed! Restore failed!");
        }
    } else {
        ErrorHandler.push("Component " + name + " is already DELETED!!! Cannot restore!");
    }
};
/**
 * Exports
 */
exports.run = RestoreComponent;