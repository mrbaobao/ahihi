/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
/**
 * Add public.ts file
 * @argument {string} name --> Name of the component
 */
var addFilePublic = function (name) {
    var pathFilePublic = Path.components + name + "/index.ts";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "component/index.ts", pathFilePublic);
    /**
     * Parse Component name
     * replace syntax camel name to component name in Camel
     * replace syntax snake name to component name in snake
     */
    var options = [[Syntax.name.camel, String.kebabToCamelBigHead(name)], [Syntax.name.kebab, name]];
    File.stringReplace(options, pathFilePublic);
};
/**
 * Add module.ts file
 * @argument {string} name --> Name of the component
 */
var addFileModule = function (name) {
    var pathFileModule = Path.components + name + "/"+ name +".module.ts";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "component/module.ts", pathFileModule);
    /**
     * Parse Component name
     * replace syntax camel name to component name in Camel
     * replace syntax snake name to component name in snake
     */
    var options = [[Syntax.name.camel, String.kebabToCamelBigHead(name)], [Syntax.name.kebab, name]];
    File.stringReplace(options, pathFileModule);
};
var AddComponent = function (name) {
    var pathComponent = Path.components + name;
    //Check component existed
    if (File.isDir(pathComponent)) {
        ErrorHandler.push("Component (" + name + ") is already existed!");
        return;
    }
    //Make component root directory
    File.makeDir(pathComponent);
    //Log
    Log.push("...running... component folder created! " + Path.components + name);
    //Create 3 folders: admin/, model/, service/
    File.makeDir(pathComponent + "/admin");
    File.makeDir(pathComponent + "/model");
    File.makeDir(pathComponent + "/service");
    //Create /public.ts file
    addFilePublic(name);
    addFileModule(name);
    //Log
    Log.push("Add new component(" + name + ") success!");
};
/**
 * Exports
 */
exports.run = AddComponent;