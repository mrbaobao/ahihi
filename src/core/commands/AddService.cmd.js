/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
/**
 * Add public.ts file
 * @argument {string} name --> Name of the component
 */
var addFileService = function (componentName, serviceName) {
    var pathFileService = Path.components + componentName + "/service/" + serviceName + ".service.ts";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "service/service.ts", pathFileService);
    Log.push("...running... service added");
    Log.push(pathFileService);
    /**
     * Parse Component name
     * replace syntax camel name to component name in Camel
     * replace syntax snake name to component name in snake
     */
    var options = [
        [Syntax.name.camel, String.kebabToCamelBigHead(serviceName)],
        [Syntax.name.kebab, serviceName]
    ];
    File.stringReplace(options, pathFileService);
    //Note
    Log.push("Note: Change property apiUrl in line ~20 of class " + pathFileService + ": " + String.kebabToCamelBigHead(serviceName));
};
/**
 * Add module.ts file
 * @argument {string} name --> Name of the component
 */
var addFileModel = function (componentName, serviceName) {
    var pathFileModel = Path.components + componentName + "/model/" + serviceName + ".model.ts";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "service/model.ts", pathFileModel);
    /**
     * Parse Component name
     * replace syntax camel name to component name in Camel
     * replace syntax snake name to component name in snake
     */
    var options = [
        [Syntax.name.camel, String.kebabToCamelBigHead(serviceName)],
        [Syntax.name.kebab, serviceName]
    ];
    File.stringReplace(options, pathFileModel);
    
};
/**
 * Import Service to Module
 * Providers
 */
var importServiceToModule = function (componentName, serviceName) {
    var pathFileModule = Path.components + componentName + "/"+ componentName +".module.ts";
    var nameService = String.kebabToCamelBigHead(serviceName) + "Service";
    //Import
    var importLine = "import {" + nameService + "} from './service/" + serviceName + ".service';";
    File.prependLine(importLine, pathFileModule);
    //Add as provider
    File.insertAfter(nameService + ",", "providers : [", pathFileModule);
};
var addFieldsToModel = function (componentName, serviceName) {
    Log.push(componentName);
    Log.push(serviceName);
};
var AddService = function (componentName, serviceName) {
    //Path to component
    var pathComponent = Path.components + componentName;
    //Check component existed
    if (!File.isDir(pathComponent)) {
        ErrorHandler.push("Component (" + componentName + ") is not existed!");
        return;
    }
    //Add file service
    addFileService(componentName, serviceName);
    //Add file model
    addFileModel(componentName, serviceName);
    //Import to module
    importServiceToModule(componentName, serviceName);
    //Add fields
    addFieldsToModel(componentName, serviceName);
    //Log
    Log.push("Add new service(" + componentName + "/" + serviceName + ") success!");
    
};
/**
 * Exports
 */
exports.run = AddService;