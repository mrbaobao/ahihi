/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
/*
Move skeleton folder to output folder
*/
var MoveSkeleton = function(pathOutput, componentName){
    var skeComFull = "component-full";
    var pathSkeletonComponent = Path.skeletons  + skeComFull;
    //Temp folder
    var pathTmp = pathOutput + Date.now() + "/";
    File.makeDir(pathTmp);
    File.copyDirectory(pathSkeletonComponent, pathTmp);
    //Rename tmp folder
    var i = 1;
    var pathComponent = pathOutput + componentName;
    if(File.isDir(pathOutput + componentName)){
        while( File.isDir(pathOutput + componentName + "-" + i) ){
            i++;
        }
        pathComponent = pathOutput + componentName + "-" + i;
    }
    File.moveDir(pathTmp, pathComponent);
    Log.push("... running ... component folder created: " + pathComponent);
    //Delete tmp folder
    File.deleteDir(pathTmp);
    //Return component folder name
    return pathComponent;
};
/*
Thông báo khi generate thành công
*/
function NotifySuccess(pathComponent, componentName){
	//Log
	Log.push("--------Completed-----------");
	Log.push("Generate component Success!!! ahihi! :)")
	Log.push("Component path: " + pathComponent);
	//Các file cần chú ý
	var arrApiFilePaths = [
		"server/src/config/routes/tin-tuc-gen-router.ts",
		"server/src/controllers/tin-tuc-gen-controller.ts",
		"server/src/middleware/tin-tuc-gen-middleware/tin-tuc-gen-schema.ts",
		"server/src/repositories/tin-tuc-gen-repository.ts"
	];
}
/*
Parsing file config
*/
var ParseConfig = function(config){
    //Parse config
    config.okmanname = config.name.replace(new RegExp("-", 'g'), "");
	if(config.apiName == undefined){
		config.apiName = config.name;
	}
	config.hasStatus = false;
	var date = new Date();
	var strDate = "(" + date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear() + ")";

	var pgConnect = config.pgConnect? config.pgConnect : "development";
    var db_col_list = "";
    var server_db_col_list = "";

    var col_list = config.col_list;
    
	var field_type;
	var status_key_value_list = "";
	var status_id_text_list = "";
	var status_id_text_list_reverse = "";
	var status_id_text_list_json = "";
	var status_sample_value = "";
	var status_value_prefix = "+";
	var status_value_suffix = "";

	var statusMap, enumClassName, arrEnum, strEnumClassList = "", api_middleware_schema = "";
	/*
	Trường hợp Sample - không có field status
	*/
	var sample_snake = "";
	var sample_value = "";
	var sample_field_label = "";
	/*
	5 default Fields 
	*/
	var defaultFields = [
		{
			"name": "time_create",
			"label": "Ngày tạo",
			"type": "Date"
		},
		{
			"name": "time_update",
			"label": "Ngày cập nhật",
			"type": "Date"
		},
		{
			"name": "user_create",
			"label": "User tạo",
			"type": "Date"
		},
		{
			"name": "user_update",
			"label": "User cập nhật",
			"type": "Date"
		},
		{
			"name": "current_status",
			"label": "Trạng thái hiện tại",
			"type": "string",
			/*
			Cấu hình enum để kiểm tra dữ liệu trong /server/---> api
			Không dùng unicode ở đây!!!!
			*/
			"enum": ["active", "delete", "pending"]
		}
		
	];
	
	//Append to columns list
	col_list = col_list.concat(defaultFields);
	var count = col_list.length;
	var countDefault = defaultFields.length;
	/*
	Tạo khai báo Class Enum cho ./server
	*/
	var createEnumClass = (name, arrEnum) => {
		var str = "export class " + name + " {\n";
		var strReturn = "", UPPERCASE;
		for(var i = 0, count = arrEnum.length; i < count; i++){
			UPPERCASE = arrEnum[i].toUpperCase();
			str += "\t" + "static readonly " + UPPERCASE + " = '"+ arrEnum[i] +"';\n";
			strReturn += "this."+ UPPERCASE;
			if(i < count - 1){
				strReturn += ", ";
			}
		}
		str += "\t" + "static get values() {\n";
		str += "\t\t" + "return ["+ strReturn +"];\n";
		str += "\t}\n";
		str += "\n}";
		return str;
	};
	/*
	Duyệt danh sách field trong database
	*/
    for(var i = 0; i < count; i++){

		field_type = col_list[i]["type"];
		api_middleware_schema += "\t\t\t" + "'"+ col_list[i]["name"] +"': {\n";
		//Primary field: id
        if(col_list[i]["isPrimary"]){
            config.id_snake = col_list[i]["name"];
            config.id_field_label = col_list[i]["label"];
        }
        //Status field
        if(col_list[i]["isStatus"]){
        	config.hasStatus = true;
            config.status_snake = col_list[i]["name"];
            config.status_kebab = config.status_snake.replace(new RegExp("_", 'g'), "-");
            config.status_camel = String.kebabToCamelBigHead(config.status_kebab);
            config.status_field_label = col_list[i]["label"];
			statusMap = col_list[i]["map"];
			for(var j = 0, countStatus = statusMap.length; j < countStatus; j++){
				status_key_value_list += "\t\t\t{" + statusMap[j][0] + ": '" + statusMap[j][1] + "'},\n";
				status_id_text_list += "\t\t{id: " + statusMap[j][0] + ", text: '" + statusMap[j][1] + "'},\n";
				//
				status_id_text_list_reverse += "'" + statusMap[j][1] + "':" + statusMap[j][0] + ",";
				status_id_text_list_json += "'" + statusMap[j][0] + "':'" + statusMap[j][1] + "',";
			}
			//Status sample value
			status_sample_value = statusMap[0][1];
			//Prefix for parse value
			if(field_type === "boolean"){
				status_value_prefix = "!!";
				status_value_suffix = "? 1 : 0";
			}
        }
        //Sample field
		if(col_list[i]["sample"] != undefined){
			sample_value = col_list[i]["sample"];
			sample_snake = col_list[i]["name"];
			sample_field_label = col_list[i]["label"];
		}
		//Enum
		if(col_list[i]["enum"] !== undefined){
			enumClassName = String.kebabToCamelBigHead(config.apiName)
				+ String.kebabToCamelBigHead(col_list[i]["name"].replace(new RegExp("_", 'g'), "-"))
				+ "Enum";
			arrEnum = col_list[i]["enum"];
			strEnumClassList += "\n" + createEnumClass(enumClassName, arrEnum) + "\n";
			api_middleware_schema += "\t\t\t\t" + "optional: true,\n";
			api_middleware_schema += "\t\t\t\t" + "isInRangeOfValue: { options: { range: "+ enumClassName +".values }, errorMessage: `${VARIABLES.ValidatorMessage.IS_IN_RANGE_OF_VALUE} ${"+ enumClassName +".values}` }\n";
		} else {
			if(field_type == "int"){
				api_middleware_schema += "\t\t\t\t" + "optional: true, isInt: { options: { min: 1 }, errorMessage: VARIABLES.ValidatorMessage.IS_INT + ' và >= 1' }\n";
			} else if(field_type == "string"){
				api_middleware_schema += "\t\t\t\t" + "optional: true, isString: { errorMessage: VARIABLES.ValidatorMessage.IS_STRING }\n";
			} else if(field_type == "Date"){
				api_middleware_schema += "\t\t\t\t" + "optional: true, isDate: { errorMessage: VARIABLES.ValidatorMessage.IS_DATE }";
			}
		}
		if(field_type == "int"){
			field_type = "number";
		}
		api_middleware_schema += "\t\t\t" + "},\n";

        if(i < count - countDefault ){
        	server_db_col_list += "\t" + col_list[i]["name"] + ": " + field_type + ";\n";
        }
        if(col_list[i]["isStatus"] && field_type === "boolean"){
        	field_type = "number";
        }
        db_col_list += "\t" + col_list[i]["name"] + ": " + field_type + ";\n";
       
    }
    config.db_col_list = db_col_list;
    config.server_db_col_list = server_db_col_list;
	//status_key_value_list
    config.status_key_value_list = status_key_value_list;
    config.status_id_text_list = status_id_text_list;
    config.status_id_text_list_reverse = status_id_text_list_reverse;
    config.status_id_text_list_json = status_id_text_list_json;
    config.status_sample_value = status_sample_value;
    config.status_value_prefix = status_value_prefix;
    config.status_value_suffix = status_value_suffix;

    
	//For Api
	config.Api = String.kebabToCamelBigHead(config.apiName);
	config.api_kebab = config.apiName;
	config.apiokman = config.api_kebab.replace(new RegExp("-", 'g'), "");
	config.db_table_name_snake = config.db_table_name
	config.api_query_option = db_col_list.replace(new RegExp(":", 'g'), "?:")
	config.api_enum_list = strEnumClassList;
	config.api_middleware_schema = api_middleware_schema;
	/*
	Trường hợp không có field status thì dùng field sample thay thế
	*/
	config.sample_snake = sample_snake;
	config.sample_value = sample_value;
	config.sample_field_label = sample_field_label;

    //Rules
    var arrConvertRules = [
    	["###pgConnect###", pgConnect],

        ["###Camel###", String.kebabToCamelBigHead(config.name)],
        ["###kebab###", config.name],
        ["###label###", config.label],
        ["###UPPER-CASE-NAME###", config.label.toUpperCase()],
        ["###UPPER_CASE###", String.replaceAll(config.name.toUpperCase(), [["-","_"]])],
		
        ["###id_snake###", config.id_snake],
        ["###id-field-label###", config.id_field_label],
		//Status
        ["###status_snake###", config.status_snake],
        ["###status-kebab###", config.status_kebab],
        ["###status-field-label###", config.status_field_label],
		["###status-key-value-list###", config.status_key_value_list],
		["###status-id-text-list###", config.status_id_text_list],
		["###status-id-text-list-reverse###", config.status_id_text_list_reverse],
		["###status-id-text-list-json###", config.status_id_text_list_json],

        ["###StatusCamel###", config.status_camel],
        ["###status_sample_value###", config.status_sample_value],
        ["###status_value_suffix###", config.status_value_suffix],
        ["###status_value_prefix###", config.status_value_prefix],

        //Sample
        ["###sample_snake###", config.sample_snake],
        ["###sample_value###", config.sample_value],
        ["###sample-field-label###", config.sample_field_label],

        //
        ["###okman###", config.okmanname],
        ["###apiUrl###", config.apiUrl],
        ["###db-col-list###", config.db_col_list],
        ["###server-db-col-list###", config.server_db_col_list],
		//For API
		["###Api###", config.Api],
		["###api-kebab###", config.api_kebab],
		["###apiokman###", config.apiokman],
		["###api-middleware-schema###", config.api_middleware_schema],
		["###api-enum-list###", config.api_enum_list],
		["###api-query-option###", config.api_query_option],
		["###db_table_name_snake###", config.db_table_name_snake],
		["###author###", (config.author || " ") + strDate],
    ];
    return arrConvertRules;
};
function ConvertString(str, rules, hasStatus){
	//Convert
	var convertedStr = String.replaceAll(str, rules);
	
	return convertedStr;
}
var ExtraConvertString = function(hasStatus){
	return (convertedStr) =>{
		//RegEx
		var regFindBlockHasStatus = /\@\@\@if\(hasStatus\)\@\@\@([\s\S]*?)\@\@\@\/if\@\@\@/g;
		var regFindBlockNoStatus = /\@\@\@if\(!hasStatus\)\@\@\@([\s\S]*?)\@\@\@\/if\@\@\@/g;
		var regFindIfNoStatus = /\@\@\@if\(!hasStatus\)\@\@\@/g;
		var regFindIfHasStatus = /\@\@\@if\(hasStatus\)\@\@\@/g;
		var regFindEndIf = /\@\@\@\/if\@\@\@/g;

		if(hasStatus){
			/*
			Trường hợp có field status
			1. Xoá Block không có field status
			2. Xoá @@@if(hasStatus)@@@
			3. Xoá @@@/if@@@
			*/
			convertedStr = convertedStr.replace(regFindBlockNoStatus, "");
			convertedStr = convertedStr.replace(regFindIfHasStatus, "");
			convertedStr = convertedStr.replace(regFindEndIf, "");
		} else {
			/*
			Trường hợp không có field status
			1. Xoá Block có field status
			2. Xoá @@@if(!hasStatus)@@@
			3. Xoá @@@/if@@@
			*/
			convertedStr = convertedStr.replace(regFindBlockHasStatus, "");
			convertedStr = convertedStr.replace(regFindIfNoStatus, "");
			convertedStr = convertedStr.replace(regFindEndIf, "");
		}
		return convertedStr;
	}
}
var countFiles = 0;
/*
Convert folder with rules
Use function: ConvertString
*/
function ConvertFolder(pathComponent, rules, totalCount, hasStatus, componentName, pathFolder){
	var pathFileNew;
	if(pathFolder == null){
		pathFolder = pathComponent;
	}
    File.loopDir(pathFolder, function(err, pathFile, isDir) {
        if(err != null) ErrorHandler.push("Error when access tmp folder!");
		pathFileNew = ConvertString(pathFile, rules, hasStatus);
		File.rename(pathFile, pathFileNew);
		if(isDir){
			countFiles++;
			Log.push("...running... on folder: " + pathFileNew );
			ConvertFolder(pathComponent, rules, totalCount, hasStatus, componentName, pathFileNew);
			Log.push("..." + Math.round(countFiles/totalCount * 100) + "%...")
			//NotifySuccess when action completed
			if(countFiles == totalCount){
				NotifySuccess(pathComponent);
			}
		} else {
			countFiles++;
			Log.push("...running... on file: " + pathFileNew );

			File.stringReplace(rules, pathFileNew, ExtraConvertString(hasStatus));
			Log.push("..." + Math.round(countFiles/totalCount * 100) + "%...")
			//NotifySuccess when action completed
			if(countFiles == totalCount){
				var pathToStatusFolder = pathComponent + "/web/components/"+ componentName +"/admin/" + componentName + "-list/shared/com/undefined"
				if(!hasStatus){
		        	Log.push("Remove status component...");
		        	Log.push(pathToStatusFolder);
		        	File.deleteDir(pathToStatusFolder);
		        }
				NotifySuccess(pathComponent, componentName);
			}
		}
    });
};

var Generate = function (filename) {
    //Path to component
    var pathComponent = Path.templates;
    var pathFile = pathComponent + filename + ".json";
    Log.push("...running... ");
    if (File.isFile(pathFile)) {
        //Include file
        var config = File.getJSON(pathFile);
		//Parse config
		var rules = ParseConfig(config);
        //Log
        Log.push("...running... config file ./template/" + filename + ".json OK.");
        //Create output folder
        var pathOutput = Path.clientRoot + "output/";
        if(!File.isDir(pathOutput)){
            File.makeDir(pathOutput);
        }
        //
        var componentName = config.name;
        //Move Skeleton to output
        var pathComponent = MoveSkeleton(pathOutput, componentName);
        //Path to folder status
        var pathToStatusFolder = pathOutput + componentName + "/web/components/###kebab###/admin/###kebab###-list/shared/com/###status-kebab###";
      	//Log.push(pathToStatusFolder);
      	//Log.push(config.hasStatus);

        if(!config.hasStatus){
        	Log.push("Remove status component...");
        	File.deleteDir(pathToStatusFolder);
        }

        //Convert
		File.loopDirAll(pathComponent, (err, list) =>{
			if(err != null){
				ErrorHandler.push("Error when working on files...");
			} else {
				ConvertFolder(pathComponent, rules, list.length, config.hasStatus, componentName);
			}
		})
		
    } else {
        //file not found
        ErrorHandler.push("File template (" + pathFile + ") not found.");
    }
};

/**
 * Exports
 */
exports.run = Generate;