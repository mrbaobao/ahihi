/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
var getFinalActionName = function(arrActionName, actionName, componentName){
    if(actionName === undefined){
        actionName = componentName + "-" + arrActionName.join("-");
    }
    return actionName;
}
/**
 * Add html file
 */
var addFileHtml = function (componentName, actionType, arrActionName, actionName) {
    actionName = getFinalActionName(arrActionName, actionName, componentName)
    var pathFileHtml = Path.components + componentName + "/" + arrActionName.join("/") + "/"+ actionName +".template.html";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "actions/" + actionType + "/template.html", pathFileHtml);
};
/**
 * Add style.scss file
 */
var addFileStyle = function (componentName, actionType, arrActionName, actionName) {
    actionName = getFinalActionName(arrActionName, actionName, componentName)
    var pathFileStyle = Path.components + componentName + "/" + arrActionName.join("/") + "/"+ actionName +".style.scss";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "actions/" + actionType + "/style.scss", pathFileStyle);
};
var addFileTs = function (componentName, actionType, arrActionName, actionName) {
    actionName = getFinalActionName(arrActionName, actionName, componentName)
    var pathFileTs = Path.components + componentName + "/" + arrActionName.join("/") + "/" +
            actionName + ".component.ts";
    //Copy File from Skeleton
    File.copy(Path.skeletons + "actions/" + actionType + "/component.ts", pathFileTs);
    /**
     * Parse Component name
     * replace syntax camel name to component name in Camel
     * replace syntax snake name to component name in snake
     */
    var options = [
        [Syntax.name.camel, String.kebabToCamelBigHead(componentName)],
        [Syntax.name.kebab, componentName],
        [Syntax.name.kebabAction, actionName],
        [Syntax.name.camelAction, String.kebabToCamelBigHead(actionName)]
    ];
    File.stringReplace(options, pathFileTs);
};
/**
 * Import Action to Module
 * Providers
 */
var importActionToModule = function (componentName, actionType, arrActionName, actionName) {
//    Log.push(actionName);
    if(actionName === undefined){
        actionName = componentName + "-" + arrActionName.join("-");
    }
//    Log.push(actionName);
    var pathFileModule = Path.components + componentName + "/"+ componentName +".module.ts";
    var nameActionClass = String.kebabToCamelBigHead(actionName) + "Component";
    //Import
    var importLine = "import {" + nameActionClass + "} from './" + arrActionName.join("/") + "/" +
            actionName + ".component';";
    File.prependLine(importLine, pathFileModule);
    //Add as provider
    File.insertAfter(nameActionClass + ",", "declarations : [", pathFileModule);
};
var isActionType = function(actionType){
    var arrActionTypes = [
        "detail",
        "search",
        "list",
        "change",
        "add",
        "update",
        "delete",
        "order"
    ];
    return arrActionTypes.indexOf(actionType) >= 0;
};
var AddAction = function (componentName, actionType, arrActionName, actionName) {
    //Path to component
    var pathComponent = Path.components + componentName + "/";
    //Check component existed
    if (!File.isDir(pathComponent)) {
        ErrorHandler.push("Component (" + componentName + ") is not existed!");
        return;
    }
    if (!isActionType(actionType)){
        ErrorHandler.push("Action type (" + actionType + ") is not available!");
        return;
    }
    var pathToAction = pathComponent + arrActionName.join("/") + "/";
    //Make dir for action
    File.makeDir(pathToAction);
    //Add file ts action
    addFileTs(componentName, actionType, arrActionName, actionName);
    //Add file html action
    addFileHtml(componentName, actionType, arrActionName, actionName);
    //Add file model
    addFileStyle(componentName, actionType, arrActionName, actionName);
    //Import to module
    importActionToModule(componentName, actionType, arrActionName, actionName);
    //Log
    Log.push("Add new action(" + componentName + "/" + arrActionName.join("/") + "/"+
            (actionName ? actionName : "") + ") success!");
};
/**
 * Exports
 */
exports.run = AddAction;