/* global process */

"use strict";
/**
 * Command
 */
/**
 * File
 */
var File = require("../File");
/**
 * String Class
 */
var String = require("../String");
/**
 * Path
 */
var Path = require("../Path");
/**
 * Error handler
 */
var ErrorHandler = require("../ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("../Log");
/**
 * 
 * @type Module Syntax|Module Syntax
 */
var Syntax = require("../Syntax");
var removeImportFromModule = function(pathToAction, actionName){
    var arrActionName = pathToAction.split("/");
    var componentName = arrActionName.shift();
    var pathFileModule = Path.components + componentName + "/"+ componentName +".module.ts";
    if(actionName === undefined){
        actionName = componentName + "-" + arrActionName.join("-");
    }
    var nameActionFull = String.kebabToCamelBigHead( actionName) + "Component";
    //Import
    var importLine = "import {" + nameActionFull + "} from './" + arrActionName.join("/") + "/" +
            actionName + ".component';";
    File.removeLine(importLine, pathFileModule);
    //Remove from providers
    var strProvider = nameActionFull + " ,";
    File.removeString(strProvider, pathFileModule);
    //In case declare service in provider without comma (,)
    File.removeString(nameActionFull, pathFileModule);
};
var RemoveAction = function (pathToAction, actionName) {
    var pathDirAction = Path.components + pathToAction;
    if (!File.isDir(pathDirAction))
    {
        Log.push("Action " + pathToAction + " is not existed.");
    } else {
        File.deleteDir(pathDirAction);
    }
    //Remove from module
    removeImportFromModule(pathToAction, actionName);
    //Log
    Log.push("Remove action(" + pathToAction + ") success!");
};
/**
 * Exports
 */
exports.run = RemoveAction;