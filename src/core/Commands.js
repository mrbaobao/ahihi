/* global process */

"use strict";


var addComponent = require("./commands/AddComponent.cmd");
var addAction = require("./commands/AddAction.cmd");
var addService = require("./commands/AddService.cmd");
var removeService = require("./commands/RemoveService.cmd");
var removeAction = require("./commands/RemoveAction.cmd");
var removeComponent = require("./commands/RemoveComponent.cmd");
var restoreComponent = require("./commands/RestoreComponent.cmd");
//
var generate = require("./commands/Generate.cmd");

/**
 * Exports
 */
exports.addComponent = addComponent.run;
exports.addAction = addAction.run;
exports.addService = addService.run;
exports.removeService = removeService.run;
exports.removeAction = removeAction.run;
exports.removeComponent = removeComponent.run;
exports.restoreComponent = restoreComponent.run;
//Generate
exports.generate = generate.run;