/* global process */

"use strict";
/**
 * Command
 */
//Can comment in json files
require('json-comments');
/**
 * String Class
 */
var Str = require("./String");
/**
 * Error handler
 */
var ErrorHandler = require("./ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("./Log");
/**
 * @param {Str} name --> name of component: landing-page, news, role, ...
 * name must be in snake form. Ex: con-ran, tin-tuc, thanh-vien, bao-cao-su-co
 */
var Cmd = require("./Commands");
/**
 * @param {Str} actionType --> name of action: list, detail, delete, search, ...
 */
var addAction = function (actionType, pathToAction, actionName) {
    if (pathToAction === undefined){
        ErrorHandler.push("Not found path to action!");
    }
    var arrName = pathToAction.split("/"), count = arrName.length;
    var componentName, arrActionName;
    if(count >= 2){
        componentName = arrName[0];
        arrName.shift();
        arrActionName = arrName;
    } else {
        ErrorHandler.push("Action name ("+ pathToAction +") is not valid! Ation name must be <component-name/path-to-action-name>");
        return;
    }
    //Add action
    Cmd.addAction(componentName, actionType, arrActionName, actionName);
};
var addService = function (name) {
    var arrName = name.split("/"), count = arrName.length;
    var componentName, serviceName;
    if(count === 1 ){
        componentName = serviceName = arrName[0];
    } else if(count === 2){
        componentName = arrName[0];
        serviceName = arrName[1];
    } else {
        ErrorHandler.push("Service name ("+ name +") is not valid! Service name must be <service-name> OR <component-name/service-name>");
        return;
    }
    //Add service
    Cmd.addService(componentName, serviceName);
};
var removeService = function (name) {
    var arrName = name.split("/"), count = arrName.length;
    var componentName, serviceName;
    if(count === 1 ){
        componentName = serviceName = arrName[0];
    } else if(count === 2){
        componentName = arrName[0];
        serviceName = arrName[1];
    } else {
        ErrorHandler.push("Service name ("+ name +") is not valid! Service name must be <service-name> OR <component-name/service-name>");
        return;
    }
    //Remove service
    Cmd.removeService(componentName, serviceName);
};
var removeAction = function (pathToAction, actionName) {
    //Remove action
    Cmd.removeAction(pathToAction, actionName);
};
/**
 * Command add new
 * @argument {Str} type --> Component | Action
 * @argument {Str} name --> name of param type
 * 
 */
var addCommand = function (type, name, params) {
    switch (type) {
        case "com":
        case "component":
            if (Str.isName(name, "snake")) {
                Cmd.addComponent(name);
            } else {
                ErrorHandler.push("Component name is not valid. The name must be in snake form. Ex: con-ran-mau-xanh.");
            }
            break;
        case "service":
            /**
             * Add new service
             * name = <component-name>/<service-name>
             * OR
             * name = <service-name> ----> In this case: <component-name> = <service-name>
             */
            addService(name);
            break;
        case "action":
            addAction(name, params[3], params[4]);
            break;
        default:
            ErrorHandler.push("Type " + type + " not found!");
    }
};
var removeCommand = function (type, name, params) {
    switch (type) {
        case "com":
        case "component":
            if (Str.isName(name, "snake")) {
                Cmd.removeComponent(name);
            } else {
                ErrorHandler.push("Component name is not valid. The name must be in snake form. Ex: con-ran-mau-xanh.");
            }
            break;
        case "action":
            //params[3] = pathToAction
            //params[4] = actionName --> maybe undefined
            removeAction(params[2], params[3]);
            break;
        case "service":
            removeService(name);
            break;
        default:
            ErrorHandler.push("Type " + type + " not found!");
    }
};
var restoreCommand = function (type, name) {
    switch (type) {
        case "com":
        case "component":
            if (Str.isName(name, "snake")) {
                Cmd.restoreComponent(name);
            } else {
                ErrorHandler.push("Component name is not valid. The name must be in snake form. Ex: con-ran-mau-xanh.");
            }
            break;
        case "action":
            Log.push("restore action...");
            break;
        default:
            ErrorHandler.push("Type " + type + " not found!");
    }
};
/**
 * Generate something by filename.json
 */
var generateCommand = function(filename){
    //
    Cmd.generate(filename)
};
/**
 * Init
 */
var run = function () {
    /**
     * Params from command line
     Example: Command = "ahh add component landing-page"
     -->
     process.argv =     [ 'C:\\Program Files\\nodejs\\node.exe',
     'C:\\Program Files\\nodejs\\node_modules\\ahh\\bin\\ahh.js',
     'add',
     'component',
     'landing-page']
     So, remove two first params
     */
    var params = process.argv.slice(2);
    //Command
    var command = params[0];
    //Type
    var type = params[1];
    //Name
    var name = params[2];
    switch (command) {
        case "add":
            //Add
            addCommand(type, name, params);
            break;
        case "del":
        case "delete":
            //Delete
            removeCommand(type, name, params);
            break;
        case "rm":
        case "remove":
            //Delete
            removeCommand(type, name, params);
            break;
        case "res":
        case "restore":
            //Delete
            restoreCommand(type, name);
            break;
        case "gen":
        case "generate":
            /**
             * Generate
             * command:
             *      gen bao-cao --------> Generate sth with file /templates/bao-cao.json
             */
            generateCommand(params[1]);
            break;
        default:
            ErrorHandler.push("Command " + command + " not found!");
    }
};

/**
 * Exports
 */
exports.run = run;