#!/usr/bin/env node
/* global process, dest */
"use strict";


/**
 * Error handler
 */
var ErrorHandler = require("../core/ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("./Log");
var Str = require("../core/String");
/**
 * File manager
 */
var fs = require('fs');
var fse = require("fs-extra");
var os = require("os");
var JSON = require('json-comments');
/**
 * Copy file
 */
var copyFile = function (source, dest) {
    fs.copyFileSync(source, dest);
};
/**
 * Copy Directory
 */
var copyDirectory = function (source, dest) {
    fse.copySync(source, dest);
};

/**
 * Copy recursively from source to dest
 * @argument {string} source
 * @argument {string} dest
 */
var copy = function (source, dest) {
    var stats = fs.statSync(source);
    if (stats.isFile()) {
        copyFile(source, dest);
    } else if (stats.isDirectory) {
        copyDirectory(source, dest);
    } else {
        ErrorHandler.push("@File: source(" + source + ") is not valid!");
    }
};
/**
 * Make Directory
 * @argument {string} pathDir --> Directory path
 */
var makeDir = function (pathDir) {
    if (!fs.existsSync(pathDir)) {
        fse.mkdirpSync( pathDir );
    } else {
        ErrorHandler.warning("Directory(" + pathDir + ") is already existed!");
    }
};
/**
 * Delete directory recursively
 */
var deleteDir = function (path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach(function (file, index) {
            var curPath = path + "/" + file;
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteDir(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};
var deleteFile = function (pathFile) {
    fs.unlinkSync(pathFile);
};
/**
 * Move dir
 */
var moveDir = function (oldPath, newPath) {
    fse.moveSync(oldPath, newPath);
};
var rename = function (oldPath, newPath) {
    fs.renameSync(oldPath, newPath);
};

var isDir = function (pathDir) {
    try {
        var s = fs.statSync(pathDir);
        return s.isDirectory();
    } catch (e) {
        return false;
    }
};
var isFile = function (pathFile) {
    try {
        var s = fs.statSync(pathFile);
        return s.isFile();
    } catch (e) {
        return false;
    }
};
/**
 * Replace string to another in file contents
 * @argument {JSON} options --> [ [str1, strReplace1], [str2, strReplace2] ]
 */
var stringReplace = function (options, pathFile, extraModify) {
    var result = fs.readFileSync(pathFile, 'utf8');
    var regEx;
    for (var i = 0, count = options.length; i < count; i++) {
        regEx = new RegExp(options[i][0], "g");
        result = result.replace(regEx, options[i][1]);
    }
    if(extraModify !== undefined){
        result = extraModify(result);
    }
    fs.writeFileSync(pathFile, result, 'utf8');
};
/**
 * Prepend a line string to a file contents
 */
var prependLine = function (line, pathFile) {
    var result = fs.readFileSync(pathFile, 'utf8');
    fs.writeFileSync(pathFile, line + os.EOL + result, 'utf8');
};
/**
 * Insert a line string after a str
 * Note: The space in param str can be space, multi-space or tab, end of line, ...
 */
var insertAfter = function (strInsert, strFind, pathFile) {
    var result = fs.readFileSync(pathFile, 'utf8');
    var _str = Str.addSplashToSpecialChars(strFind);
    var arrStr = _str.split(" "), pattern = "";
    for (var i = 0, count = arrStr.length; i < count; i++) {
        pattern += arrStr[i];
        if (i < count - 1) {
            pattern += "([\\s]*)";
        }
    }
    var regEx = new RegExp(pattern, "i");
    result = result.replace(regEx, strFind + " " + strInsert);
    //Write file
    fs.writeFileSync(pathFile, result, 'utf8');
};
var insertLineAfter = function (line, str, pathFile) {
    insertAfter(os.EOL + line, str, pathFile);
};
var removeString = function (str, pathFile) {
    var result = fs.readFileSync(pathFile, 'utf8');
    var _str = Str.addSplashToSpecialChars(str);
    var arrStr = _str.split(" "), pattern = "([\\s]*)";
    for (var i = 0, count = arrStr.length; i < count; i++) {
        pattern += arrStr[i];
        if (i < count - 1) {
            pattern += "([\\s]*)";
        }
    }
    var regEx = new RegExp(pattern + "([\\s]*)");
    result = result.replace(regEx, "");
    //Write file
    fs.writeFileSync(pathFile, result, 'utf8');
};
var removeLine = function (line, pathFile) {
    var result = fs.readFileSync(pathFile, 'utf8');
    var _line = Str.addSplashToSpecialChars(line);
    var arrStr = _line.split(" "), pattern = "([\\s]*)";
    for (var i = 0, count = arrStr.length; i < count; i++) {
        pattern += arrStr[i];
        if (i < count - 1) {
            pattern += "([\\s]*)";
        }
    }
//    Log.push(pattern);
    var regEx = new RegExp(pattern + "([\\s]*)");
    result = result.replace(regEx, "");
    //Write file
    fs.writeFileSync(pathFile, result, 'utf8');
};
var getJSON = function(pathFile){
    var content = fs.readFileSync(pathFile, "utf8");
    // parse JSON String 
    return JSON.parse(content);
};

/**
 * Explores recursively a directory and returns all the filepaths and folderpaths in the callback.
 * 
 * @see http://stackoverflow.com/a/5827895/4241030
 * @param {String} dir 
 * @param {Function} done 
 */
const path = require('path');
function loopDir(dir, done) {
    let results = [];
    fs.readdir(dir, function(err, list) {
        if (err) return done(err);
        var pending = list.length;
        if (!pending) return done(null, results);
        list.forEach(function(file){
            file = path.resolve(dir, file);
            fs.stat(file, function(err, stat){
                // If directory, execute a recursive call
                if (stat && stat.isDirectory()) {
                    done(null, file, true)
                } else {
                    done(null, file, false)
                }
            });
        });
    });
};

/**
 * Explores recursively a directory and returns all the filepaths and folderpaths in the callback.
 * 
 * @see http://stackoverflow.com/a/5827895/4241030
 * @param {String} dir 
 * @param {Function} done 
 */
function filewalker(dir, done) {
    let results = [];

    fs.readdir(dir, function(err, list) {
        if (err) return done(err);

        var pending = list.length;

        if (!pending) return done(null, results);

        list.forEach(function(file){
            file = path.resolve(dir, file);

            fs.stat(file, function(err, stat){
                // If directory, execute a recursive call
                if (stat && stat.isDirectory()) {
                    // Add directory to array [comment if you need to remove the directories from the array]
                    results.push(file);

                    filewalker(file, function(err, res){
                        results = results.concat(res);
                        if (!--pending) done(null, results);
                    });
                } else {
                    results.push(file);

                    if (!--pending) done(null, results);
                }
            });
        });
    });
};


/**
 * exports
 */
//Copy from source to dest
exports.copy = copy;
//Create a directory
exports.makeDir = makeDir;
//Move a directory from source to dest
exports.moveDir = moveDir;
exports.rename = rename;
//Delete directory
exports.deleteDir = deleteDir;
exports.deleteFile = deleteFile;
exports.isDir = isDir;
exports.isFile = isFile;
//Copy directory
exports.copyDirectory = copyDirectory;
//String replace
exports.stringReplace = stringReplace;
exports.prependLine = prependLine;
exports.insertLineAfter = insertLineAfter;
exports.insertAfter = insertAfter;
exports.removeString = removeString;
exports.removeLine = removeLine;
exports.getJSON = getJSON;
exports.loopDir = loopDir;
exports.loopDirAll = filewalker;