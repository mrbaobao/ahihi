/* global process */

"use strict";
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("./Log");
/**
 * Error handler
 */
/**
 * 
 * @param {type} msg
 * @returns {undefined}
 */
var push = function (msg) {
    //Log error
    Log.push(new Error(msg));
};
var warning = function (msg) {
    //Log error
    Log.push("Warning!! " + msg);
};
/**
 * Export
 */
exports.push = push;
exports.warning = warning;