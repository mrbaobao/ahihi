"use strict";

/**
 * Error handler
 */
var ErrorHandler = require("../core/ErrorHandler");
/**
 * 
 * @type Module Log|Module Log
 */
var Log = require("./Log");
/**
 * Special chars
 */
var specialChars = [
    "[",
    "]",
    "/",
    "\\",
    "'",
    "\""
];

/**
 * Class String
 */
/**
 * Check if a string str is all upper case
 */
var isAllUpperCase = function (str) {
    return str === str.toUpperCase();
};
/**
 * Check is a string str is all lower case
 */
var isAllLowerCase = function (str) {
    return str === str.toLowerCase();
};





var isName = function (str, type) {
    switch (type) {
        case "snake":
            //Snake form string must be all lowercase and dash - character
            if (!/[a-z0-9]+/.test(str)) {
                return false;
            }
            break;
        default:
            Log.push("@String: type(" + type + ") not found!");
            return false;
    }
    //If no problem, return true
    return true;
};
/**
 * Convert snake name to Camel
 */
function kebabToCamelBigHead(s) {
    return ("-" + s).replace(/(\-\w)/g, function (m) {
        return m[1].toUpperCase();
    });
}
function addSplashToSpecialChars(str){
    str = str.replace("[", "\\[");
    str = str.replace("]", "\\]");
    str = str.replace(/\'/g, '(\"|\')');
    str = str.replace(/\"/g, "(\"|\')");
    return str;
}
function replaceAll(str, rules){
    var count = rules.length;
    for(var i = 0; i < count; i++){
        str = str.replace(new RegExp(rules[i][0], 'g'), rules[i][1]);
    }
    return str;
}
/**
 * Exports
 */
exports.isName = isName;
exports.kebabToCamelBigHead = kebabToCamelBigHead;
exports.specialChars = specialChars;
exports.addSplashToSpecialChars = addSplashToSpecialChars;
exports.replaceAll = replaceAll;
