#!/usr/bin/env node
/* global process */
"use strict";
//Require command.js
var Startup = require("../src/core/Startup.js");
//Process
Startup.run();
